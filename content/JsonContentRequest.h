#ifndef JSONCONTENTREQUEST_H
#define JSONCONTENTREQUEST_H

#include <QObject>
#include <QVariant>

class JSONFileDoc;

class JsonContentRequest : public QObject
{
    Q_OBJECT
public:
    explicit JsonContentRequest(QObject* parent = nullptr);
    ~JsonContentRequest() override;

    QVariant requestObject(const QString& path);
    QVariant requestArray(const QString& path);

private:
    JSONFileDoc* m_jsoner;
};

#endif // JSONCONTENTREQUEST_H
