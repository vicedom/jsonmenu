#ifndef CPPLOADER_H
#define CPPLOADER_H

#include <QQuickItem>
#include <QQmlContext>
#include <QQmlEngine>
//#include <QFileSystemWatcher>
#include <QMetaType>

class JsonContentRequest;
class JsonDataRequest;
class FileChangeWatcher;

namespace simulas {
    class JsonDataPushSimulator;
}

class CppLoader : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QVariant holder READ holder WRITE setHolder NOTIFY holderChanged)

public:
    explicit CppLoader(QObject* parent = nullptr);
    ~CppLoader() override;

    QVariant holder() const;
    void setHolder(const QVariant& val);

    Q_INVOKABLE void initContainer(QObject* obj);
    Q_INVOKABLE void registerPopup(QObject* obj);
    Q_INVOKABLE void initContents(const QString& command, const QString& source, const QString& use, const QVariant& args = QVariant());
    Q_INVOKABLE void initModels(QObject* obj, const QString& source, const QString& file);

signals:
    void holderChanged();

private:
    void initPatholino(const QVariant& varRoot);
    void initMenulino(const QVariant& varRoot);
    void initContento(const QVariant& varRoot);
    void initPopolino(const QVariant& varRoot, const QVariant& args);

    static QQuickItem* unfoldJsonObject(QQmlContext* parentContext, const QVariant& varObject);
    static void fillProps(QQuickItem* item, const QVariant& varProps);
    static void fillComps(QQuickItem* item, const QVariant& varComps, QQmlEngine* engine);
    static void fillModels(QQuickItem* item, const QVariant& varJsonModel);
    static void fillModels(const QVariant& varJsonModel, QObject* obj);
    static QVariant resolveModelValue(QQuickItem* item, const QVariantMap& modelMap, QString& fileSubPath);
    static QVariant resolveModelValue(const QVariantMap& modelMap, QObject* obj, QString& fileSubPath);
    static QVariant resoveModelDelegates(QQuickItem* item, const QVariant& varJsonArray);
    static QVariant heldModel(const QString& hirarchy);

    static QVariantMap objectMapForJsonObject(const QVariant& varObject);

private:
    QQuickItem* m_container;
    QQuickItem* m_patholino;
    QQuickItem* m_menulino;
    QQuickItem* m_contento;
    QObject* m_popolino;

    static QObject* m_holder;
    static JsonContentRequest* m_contentRequest;
    static JsonDataRequest* m_dataRequest;    
    static simulas::JsonDataPushSimulator* m_dataPushSimulator;
    static FileChangeWatcher* m_fileWatcher;
};

Q_DECLARE_METATYPE(CppLoader*)

#endif // CPPLOADER_H
