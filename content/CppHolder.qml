pragma Singleton
import QtQuick 2.11
import de.techdrivers.bernina 1.0
import "."

QtObject {
    id: object
    objectName: "CppHolder"

    property var loader: CppLoader {
        id: cppLoader
        objectName: "cppLoader"
        holder: object
    }

    function returnEval(hirarchy)
    {
        // @disable-check M23
        var res = eval(hirarchy);
        return res;
    }

    property var globalModelBase: null
}
