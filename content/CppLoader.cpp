#include "CppLoader.h"
#include "JsonContentRequest.h"
#include "JsonDataRequest.h"
#include "JSONFileBinding.h"
#include "JsonDataPushSimulator.h"
#include "FileChangeWatcher.h"

#include <QQmlEngine>
#include <QQmlComponent>
#include <QQmlError>
#include <QRandomGenerator>

#include <QDebug>

QObject* CppLoader::m_holder = nullptr;
JsonDataRequest* CppLoader::m_dataRequest = nullptr;
JsonContentRequest* CppLoader::m_contentRequest = nullptr;
simulas::JsonDataPushSimulator* CppLoader::m_dataPushSimulator = nullptr;
FileChangeWatcher* CppLoader::m_fileWatcher = nullptr;

CppLoader::CppLoader(QObject* parent)
    : QObject(parent)
    , m_container(nullptr)
    , m_patholino(nullptr)
    , m_menulino(nullptr)
    , m_contento(nullptr)
    , m_popolino(nullptr)
{
    setObjectName("CppLoader");

    m_dataRequest = new JsonDataRequest;
    m_contentRequest = new JsonContentRequest;
    m_dataPushSimulator = new simulas::JsonDataPushSimulator;
    m_fileWatcher = new FileChangeWatcher;
}

CppLoader::~CppLoader()
{

}

QVariant CppLoader::holder() const
{
    QVariant result;
    result.setValue<QObject*>(m_holder);
    return result;
}

void CppLoader::setHolder(const QVariant& val)
{
    QObject* obj = val.value<QObject*>();

    if (obj != m_holder) {

        m_holder = obj;
        emit holderChanged();
    }
}

QVariant CppLoader::heldModel(const QString& hirarchy)
{
    QVariant result;
    result.setValue<QObject*>(nullptr);
    QVariant varHirarchy(hirarchy.trimmed());


    if (!m_holder || hirarchy.trimmed().isEmpty()) {
        return result;
    }


    bool done = QMetaObject::invokeMethod(m_holder, "returnEval", Q_RETURN_ARG(QVariant, result), Q_ARG(QVariant, varHirarchy));
    Q_UNUSED(done);

    return result;
}

void CppLoader::initModels(QObject* obj, const QString& source, const QString& file)
{
    QString path = source;
    path += source.isEmpty() ? "" : "/";
    path += file;

    QVariant varObject = m_dataRequest->requestObject(path);
    QVariantMap objectMap = objectMapForJsonObject(varObject);

    QVariant varModel = objectMap.value("models");
    fillModels(varModel, obj);
}

void CppLoader::initContainer(QObject* obj)
{
    m_container = qobject_cast<QQuickItem*>(obj);
    m_patholino = obj->property("patholino").value<QQuickItem*>();
    m_menulino = obj->property("menulino").value<QQuickItem*>();
    m_contento = obj->property("contento").value<QQuickItem*>();

    initContents("root", "settings", "");
}

void CppLoader::registerPopup(QObject* obj)
{
    m_popolino = obj;
}

void CppLoader::initContents(const QString& command, const QString& source, const QString& use, const QVariant& args)
{
    QVariant varSource(source);
    QVariant varUse(use);
    QVariant varPart;
    QVariantMap varPartMap;

    QVariant varRoot = m_contentRequest->requestObject(command);
    if (!varRoot.isValid()) {
        return;
    }
    QVariantMap varRootMap = varRoot.value<QVariantMap>();

    QVariantMap::iterator it = varRootMap.begin();

    while (it != varRootMap.end()) {

        if (it.value().type() == static_cast<QVariant::Type>(QMetaType::QVariantMap)) {

            varPartMap = it.value().value<QVariantMap>();

            if (varPartMap.contains("source")) {
                varPartMap["source"] = varSource;
            }
            if (!varPartMap.contains("content") && !use.isEmpty()) {
                varPartMap["content"] = varUse;
            }

            varPart.setValue<QVariantMap>(varPartMap);
            it->setValue(varPart);
        }

        ++it;
    }

    varRoot.setValue<QVariantMap>(varRootMap);

    initPatholino(varRoot);
    initMenulino(varRoot);
    initContento(varRoot);
    initPopolino(varRoot, args);
}

void CppLoader::initPatholino(const QVariant& varRoot)
{
    QVariant varPatholino = varRoot.value<QVariantMap>().value("patholino");
    QQmlContext* patholinoContext = QQmlEngine::contextForObject(m_patholino);

    QQuickItem* patholinoInlay = unfoldJsonObject(patholinoContext, varPatholino);

    if (patholinoInlay) {

        patholinoInlay->setParentItem(m_patholino);
        patholinoInlay->setParent(m_patholino);

        QVariant varInlay;
        varInlay.setValue<QQuickItem*>(patholinoInlay);
        m_patholino->setProperty("inlay", varInlay);
    }
}

void CppLoader::initMenulino(const QVariant& varRoot)
{
    QVariant varMenulino = varRoot.value<QVariantMap>().value("menulino");
    QQmlContext* menulinoContext = QQmlEngine::contextForObject(m_menulino);

    QQuickItem* menulinoInlay = unfoldJsonObject(menulinoContext, varMenulino);

    if (menulinoInlay) {

        menulinoInlay->setParentItem(m_menulino);
        menulinoInlay->setParent(m_menulino);

        QVariant varInlay;
        varInlay.setValue<QQuickItem*>(menulinoInlay);
        m_menulino->setProperty("inlay", varInlay);
    }
}

void CppLoader::initContento(const QVariant& varRoot)
{
    QVariant varContento = varRoot.value<QVariantMap>().value("contento");
    QQmlContext* contentoContext = QQmlEngine::contextForObject(m_contento);

    QQuickItem* contentoInlay = unfoldJsonObject(contentoContext, varContento);

    if (contentoInlay) {

        contentoInlay->setParentItem(m_contento);
        contentoInlay->setParent(m_contento);

        QVariant varInlay;
        varInlay.setValue<QQuickItem*>(contentoInlay);
        m_contento->setProperty("inlay", varInlay);
    }
}

void CppLoader::initPopolino(const QVariant& varRoot, const QVariant& args)
{
    QVariant varPopolino = varRoot.value<QVariantMap>().value("popolino");
    QQmlContext* popolinoContext = QQmlEngine::contextForObject(m_popolino);

    QQuickItem* popolinoInlay = unfoldJsonObject(popolinoContext, varPopolino);

    if (popolinoInlay) {

        popolinoInlay->setParent(m_popolino);

        QMetaObject::invokeMethod(popolinoInlay, "setArgs", Q_ARG(QVariant, args));

        QVariant varInlay;
        varInlay.setValue<QQuickItem*>(popolinoInlay);
        m_popolino->setProperty("inlay", varInlay);
    }
}

QQuickItem* CppLoader::unfoldJsonObject(QQmlContext* parentContext, const QVariant& varObject)
{
    QVariantMap varObjectMap = objectMapForJsonObject(varObject);

    QString file = varObjectMap.value("file").toString();
    if (file.isEmpty()) {
        return nullptr;
    }

    QString source = varObjectMap.value("source").toString();
    QString path = "qrc:/" + source;
    path += source.isEmpty() ? "" : "/";
    path += file + ".qml";

    QQmlComponent component(parentContext->engine());
    component.loadUrl(path);
    QQmlContext* context = new QQmlContext(parentContext);
    QQuickItem* item = qobject_cast<QQuickItem*>(component.create(context));

    if (!item) {

        QList<QQmlError> errorList = component.errors();

        if (errorList.length() > 0) {
            qDebug() << errorList;
        }

        delete context;
        return nullptr;
    }

    context->setParent(item);
    fillProps(item, varObjectMap.value("props"));    
    fillModels(item, varObjectMap.value("models"));

    QQmlEngine::setObjectOwnership(item, QQmlEngine::JavaScriptOwnership);
    return item;
}

void CppLoader::fillProps(QQuickItem* item, const QVariant& varProps)
{
    if (!item || !varProps.isValid()) {
        return;
    }

    QVariantMap varPropsMap = objectMapForJsonObject(varProps);
    QVariantMap::const_iterator it = varPropsMap.constBegin();

    while (it != varPropsMap.constEnd()) {

        if (it.value().type() != static_cast<QVariant::Type>(QMetaType::QVariantMap)) {
            item->setProperty(qPrintable(it.key()), it.value());
        }
        else {

            QQuickItem* subItem = unfoldJsonObject(QQmlEngine::contextForObject(item), it.value());

            if (subItem) {

                subItem->setParentItem(item);
                subItem->setParent(item);
                QVariant varSubItem;
                varSubItem.setValue<QQuickItem*>(subItem);
                item->setProperty(qPrintable(it.key()), varSubItem);
            }
            else {
                item->setProperty(qPrintable(it.key()), it.value());
            }
        }

        ++it;
    }
}

void CppLoader::fillModels(QQuickItem* item, const QVariant& varJsonModel)
{
    if (!item || varJsonModel.type() != static_cast<QVariant::Type>(QMetaType::QVariantList)) {
        return;
    }

    QVariantList modelsList = varJsonModel.value<QVariantList>();
    int i, len = modelsList.length();

    QVariant varModel;
    QVariantMap modelMap;
    QString setProp, path;
    QVariant varSimulas;

    for (i = 0; i < len; i++) {

        varModel = modelsList.at(i);
        modelMap = varModel.value<QVariantMap>();

        setProp = modelMap.value("set").toString();

        varSimulas = modelMap.value("simulas");

        QVariant varValue = resolveModelValue(item, modelMap, path);
        item->setProperty(qPrintable(setProp), varValue);

        if (varSimulas.type() == static_cast<QVariant::Type>(QMetaType::QVariantMap)) {

            QVariantMap simulasMap = varSimulas.value<QVariantMap>();
            QString pushType = simulasMap.value("type").toString();

            if (pushType == "index") {

                QString push = simulasMap.value("push").toString();
                int max = simulasMap.value("max").toInt();
                int refresh = simulasMap.value("refresh").toInt();

                if (max > 1) {
                    m_dataPushSimulator->registerArrayIndexItem(item,
                                                                m_dataRequest->jsonDataFilePath(path),
                                                                push,
                                                                max,
                                                                refresh);
                }
            }
        }
    }
}

void CppLoader::fillModels(const QVariant& varJsonModel, QObject* obj)
{
    if (!obj || varJsonModel.type() != static_cast<QVariant::Type>(QMetaType::QVariantList)) {
        return;
    }

    QVariantList modelsList = varJsonModel.value<QVariantList>();
    int i, len = modelsList.length();

    QVariant varModel;
    QVariantMap modelMap;
    QString setProp, path;
    QVariant varSimulas;
    QVariant varValue;

    for (i = 0; i < len; i++) {

        varModel = modelsList.at(i);
        modelMap = varModel.value<QVariantMap>();

        setProp = modelMap.value("set").toString();

        varSimulas = modelMap.value("simulas");

        varValue = resolveModelValue(modelMap, obj, path);
        obj->setProperty(qPrintable(setProp), varValue);

        if (varSimulas.type() == static_cast<QVariant::Type>(QMetaType::QVariantMap)) {

            QVariantMap simulasMap = varSimulas.value<QVariantMap>();
            QString pushType = simulasMap.value("type").toString();

            if (pushType == "index") {

                QString push = simulasMap.value("push").toString();
                int max = simulasMap.value("max").toInt();
                int refresh = simulasMap.value("refresh").toInt();

                if (max > 1) {
                    m_dataPushSimulator->registerArrayIndexItem(obj,
                                                                m_dataRequest->jsonDataFilePath(path),
                                                                push,
                                                                max,
                                                                refresh);
                }
            }
        }
    }
}

QVariant CppLoader::resolveModelValue(QQuickItem* item, const QVariantMap& modelMap, QString& fileSubPath)
{
    QVariant result;
    QVariant varData = modelMap.value("data");
    QVariant varBinding = modelMap.value("binding");
    QVariant varReference = modelMap.value("reference");

    if (varData.isValid()) {

        QVariantMap dataMap = varData.value<QVariantMap>();
        QString get = dataMap.value("get").toString();
        QString source = dataMap.value("source").toString();

        QString path = source;
        path += source.isEmpty() ? "" : "/";
        path += get;

        QVariant varModelData = m_dataRequest->requestObject(path);

        if (varModelData.type() == static_cast<QVariant::Type>(QMetaType::QVariantMap)) {

            QVariantMap modelDataMap = varModelData.value<QVariantMap>();
            result = resoveModelDelegates(item, modelDataMap.value("value"));
            fileSubPath = path;
        }
    }
    else if (varBinding.isValid()) {

        QVariantMap dataMap = varBinding.value<QVariantMap>();
        QString get = dataMap.value("get").toString();
        QString source = dataMap.value("source").toString();
        int refresh = dataMap.value("refresh").toInt();

        QString path = source;
        path += source.isEmpty() ? "" : "/";
        path += get;

        JSONFileBinding* binding = new JSONFileBinding();

        if (!binding->connectToFile(get, source, m_fileWatcher, refresh)) {
            delete binding;
        }
        else {

            if (binding->model()) {

                binding->model()->setParent(item);
                QQmlEngine::setObjectOwnership(binding->model(), QQmlEngine::JavaScriptOwnership);
                result.setValue<QObject*>(binding->model());
            }
            else {

                binding->setParent(item);
                QQmlEngine::setObjectOwnership(binding, QQmlEngine::JavaScriptOwnership);
                result.setValue<QObject*>(binding);
            }

            fileSubPath = path;
        }
    }
    else if (varReference.isValid()) {

        QVariantMap dataMap = varReference.value<QVariantMap>();
        QString get = dataMap.value("get").toString();
        QString source = dataMap.value("source").toString();

        if (source.trimmed().isEmpty() || source == "holder") {
            result = heldModel(get);
        }
    }

    return result;
}

QVariant CppLoader::resolveModelValue(const QVariantMap& modelMap, QObject* obj, QString& fileSubPath)
{
    QVariant result;
    QVariant varData = modelMap.value("data");
    QVariant varBinding = modelMap.value("binding");
    QVariant varReference = modelMap.value("reference");

    if (varData.isValid()) {

        QVariantMap dataMap = varData.value<QVariantMap>();
        QString get = dataMap.value("get").toString();
        QString source = dataMap.value("source").toString();

        QString path = source;
        path += source.isEmpty() ? "" : "/";
        path += get;

        QVariant varModelData = m_dataRequest->requestObject(path);

        if (varModelData.type() == static_cast<QVariant::Type>(QMetaType::QVariantMap)) {

            QVariantMap modelDataMap = varModelData.value<QVariantMap>();
            result = modelDataMap.value("value");
            fileSubPath = path;
        }
    }
    else if (varBinding.isValid()) {

        QVariantMap dataMap = varBinding.value<QVariantMap>();
        QString get = dataMap.value("get").toString();
        QString source = dataMap.value("source").toString();
        int refresh = dataMap.value("refresh").toInt();

        QString path = source;
        path += source.isEmpty() ? "" : "/";
        path += get;

        JSONFileBinding* binding = new JSONFileBinding();

        if (!binding->connectToFile(get, source, m_fileWatcher, refresh)) {
            delete binding;
        }
        else {

            if (binding->model()) {

                binding->model()->setParent(obj);
                QQmlEngine::setObjectOwnership(binding->model(), QQmlEngine::JavaScriptOwnership);
                result.setValue<QObject*>(binding->model());
            }
            else {

                binding->setParent(obj);
                QQmlEngine::setObjectOwnership(binding, QQmlEngine::JavaScriptOwnership);
                result.setValue<QObject*>(binding);
            }

            fileSubPath = path;
        }
    }
    else if (varReference.isValid()) {

        QVariantMap dataMap = varReference.value<QVariantMap>();
        QString get = dataMap.value("get").toString();
        QString source = dataMap.value("source").toString();

        if (source.trimmed().isEmpty() || source == "holder") {
            result = heldModel(get);
        }
    }

    return result;
}

QVariant CppLoader::resoveModelDelegates(QQuickItem* item, const QVariant& varJsonArray)
{
    if (!item || varJsonArray.type() != static_cast<QVariant::Type>(QMetaType::QVariantList)) {
        return varJsonArray;
    }

    QVariantList inputList = varJsonArray.value<QVariantList>();
    int i, len = inputList.length();

    if (len < 1) {
        return inputList;
    }

    QVariant varAct;
    QVariantMap actMap;
    QVariant varDelegate;

    QVariantList outputList;
    QQmlContext* parentContext = QQmlEngine::contextForObject(item);

    for (i = 0; i < len; i++) {

        varAct = inputList.at(i);
        actMap = varAct.value<QVariantMap>();
        varDelegate = actMap.value("delegate");

        QQuickItem* delegate = unfoldJsonObject(parentContext, varDelegate);

        if (delegate) {

            delegate->setParentItem(item);
            delegate->setParent(item);
            varAct.setValue<QQuickItem*>(delegate);
        }

        outputList.append(varAct);
    }

    QVariant result;
    result.setValue<QVariantList>(outputList);
    return result;
}

QVariantMap CppLoader::objectMapForJsonObject(const QVariant& varObject)
{
    QVariantMap result = varObject.value<QVariantMap>();

    if (!result.contains("content")) {
        return result;
    }

    QString content = result.value("content").toString();
    if (content.isEmpty()) {
        return result;
    }

    QString source = result.value("source").toString();
    QString path = "/" + source;
    path += source.isEmpty() ? "" : "/";
    path += content;

    QVariant varContent = m_contentRequest->requestObject(path);

    if (!varContent.isValid()) {
        return result;
    }

    result = varContent.value<QVariantMap>();
    return result;
}

//void CppLoader::fillItemModel(QQuickItem* item, const QVariantMap& varModelMap)
//{
//    bool readonly = !varModelMap.value("variable").toBool();
//    QVariant varData = varModelMap.value("data");

//    if (!varData.isValid()) {
//        return;
//    }

//    QVariantMap varDataMap = objectMapForJsonObject(varData);

//    QVariant varDelegate = varModelMap.value("delegate");

//    if (!varDelegate.isValid()) {
//        return;
//    }

//    QString file = varDataMap.value("get").toString();
//    if (file.isEmpty()) {
//        return;
//    }

//    QString source = varDataMap.value("source").toString();
//    QString path = "/" + source;
//    path += source.isEmpty() ? "" : "/";
//    path += file;

//    QString type = varDataMap.value("type").toString();


//    if (type == "array" && readonly) {

//        QVariant varArray = m_dataRequest->requestArray(path);

//        if (!varArray.isValid()) {
//            return;
//        }

//        QVariantList modelList = varArray.value<QVariantList>();
//        int i, len = modelList.length();

//        for (i = 0; i < len; i++) {

//            QQuickItem* modelItem = unfoldJsonObject(QQmlEngine::contextForObject(item), varDelegate);

//            if (modelItem) {

//                fillProps(modelItem, modelList.at(i));
//                QQmlEngine::setObjectOwnership(modelItem, QQmlEngine::CppOwnership);
//                modelItem->setParentItem(item);
//                QVariant varModelItem;
//                varModelItem.setValue<QQuickItem*>(modelItem);
//                item->setProperty("modelItem", varModelItem);
//            }
//        }
//    }
//}

//void CppLoader::fillValueModel(QQuickItem* item, const QVariantMap& varModelMap)
//{
//    bool readonly = !varModelMap.value("variable").toBool();
//    QVariant varData = varModelMap.value("data");

//    if (!varData.isValid()) {
//        return;
//    }

//    QVariantMap varDataMap = objectMapForJsonObject(varData);

//    QString file = varDataMap.value("get").toString();
//    if (file.isEmpty()) {
//        return;
//    }

//    QString source = varDataMap.value("source").toString();
//    QString path = "/" + source;
//    path += source.isEmpty() ? "" : "/";
//    path += file;

//    QString type = varDataMap.value("type").toString();

//    if (type == "object" && readonly) {

//        QVariant varObject = m_dataRequest->requestObject(path);

//        if (!varObject.isValid()) {
//            return;
//        }

//        fillProps(item, varObject);
//    }
//}

//void CppLoader::fillIndexModel(QQuickItem* item, const QVariantMap& varModelMap)
//{
//    bool readonly = !varModelMap.value("variable").toBool();
//    QVariant varData = varModelMap.value("data");

//    if (!varData.isValid()) {
//        return;
//    }

//    QVariantMap varDataMap = objectMapForJsonObject(varData);

//    QString file = varDataMap.value("get").toString();
//    if (file.isEmpty()) {
//        return;
//    }

//    QString source = varDataMap.value("source").toString();
//    QString path = "/" + source;
//    path += source.isEmpty() ? "" : "/";
//    path += file;

//    QString type = varDataMap.value("type").toString();
//    QString initial = varDataMap.value("initial").toString();

//    if (type == "array") {

//        QVariant varArray = m_dataRequest->requestArray(path);

//        if (!varArray.isValid()) {
//            return;
//        }

//        QVariantList modelList = varArray.value<QVariantList>();
//        int i, len = modelList.length();
//        bool ok = false;
//        i = initial.toInt(&ok);

//        if (ok) {

//            if (i < 0 || i >= len) {
//                i = 0;
//            }

//            QVariant varObject = modelList.at(i);
//            fillProps(item, varObject);
//        }
//        else if (initial == "random") {

//            i = QRandomGenerator::global()->bounded(0, len);
//            QVariant varObject = modelList.at(i);
//            fillProps(item, varObject);
//        }

//        if (!readonly) { // simulates push from data provider
//            m_dataPushSimulator->registerAsIndexedArray(item, varArray, 5000);
//        }
//    }
//}

//void CppLoader::fillComps(QQuickItem* item, const QVariant& varComps, QQmlEngine* engine)
//{
//    if (!item || varComps.type() != static_cast<QVariant::Type>(QMetaType::QVariantMap)) {
//        return;
//    }

//    QVariantMap compsMap = objectMapForJsonObject(varComps);
//    QVariantMap::const_iterator it = compsMap.constBegin();
//    QVariant varJsonObject;
//    QString compToSet;

//    while (it != compsMap.constEnd()) {

//        compToSet = it.key();
//        varJsonObject = it.value();

//        if (varJsonObject.type() == static_cast<QVariant::Type>(QMetaType::QVariantMap)) {

//            CppQmlComponent* cppComponent = new CppQmlComponent(engine);

//            cppComponent->setTopLevelJsonObject(varJsonObject);
//            QQmlEngine::setObjectOwnership(cppComponent, QQmlEngine::JavaScriptOwnership);

//            QVariant varProp;
//            varProp.setValue<QQmlComponent*>(cppComponent);
//            item->setProperty(qPrintable(compToSet), varProp);
//        }

//        ++it;
//    }
//}


