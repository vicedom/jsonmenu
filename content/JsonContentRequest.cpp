#include "JsonContentRequest.h"
#include "JSONFileDoc.h"
#include "JSONContainer.h"

#include <QDebug>

JsonContentRequest::JsonContentRequest(QObject* parent)
    : QObject(parent)
    , m_jsoner(nullptr)
{
    m_jsoner = new JSONFileDoc(this);
}

JsonContentRequest::~JsonContentRequest()
{

}

QVariant JsonContentRequest::requestObject(const QString& path)
{
    JSONContainer* res = m_jsoner->read("/content/" + path);
    QVariant result;

    if (res->type() == JSONContainer::ERROR) {
        qDebug() << res->data().toString();
    }
    else if (res->type() != JSONContainer::OBJECT) {
        qDebug() << "Invalid content requesttype <" << path << "> is not an object";
    }
    else {
        result = res->data();
    }

    delete res;
    return result;
}

QVariant JsonContentRequest::requestArray(const QString& path)
{
    JSONContainer* res = m_jsoner->read("/content/" + path);
    QVariant result;

    if (res->type() == JSONContainer::ERROR) {
        qDebug() << res->data().toString();
    }
    else if (res->type() != JSONContainer::ARRAY) {
        qDebug() << "Invalid content requesttype <" << path << "> is not an array";
    }
    else {
        result = res->data();
    }

    delete res;
    return result;
}
