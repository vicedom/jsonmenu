pragma Singleton
import QtQuick 2.11
import QtMultimedia 5.9
import "."

QtObject {
    id: object
    objectName: "SoundMachine"

    property var player: MediaPlayer {
        id: mediaPlayer
        objectName: "mediaPlayer"

        autoPlay: true
        volume: (at) ? at.volume : 1.0
        muted: (at) ? at.muted : false

        onStopped: stopConsument()
    }

    property var at: QtModel.mainAppModel.subAppSettings.general.soundmachine

    function actConsumer()
    {
        return consument;
    }

    function playForMe(src, csm, func) {

        if (consument) {

            if (consument !== csm) {
                csm[func]();
            }

            return;
        }

        consument = csm;
        consumentStopFunc = func;
        player.source = src;
    }

    function stopPlay()
    {
        player.stop();
    }

    property var consument: null
    property string consumentStopFunc: ""

    function stopConsument()
    {
        if (consument) {
            consument[consumentStopFunc]();
        }

        consument = null;
        consumentStopFunc = "";
        player.source = "";
    }

}
