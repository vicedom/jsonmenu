import QtQuick 2.11
import "."

Canvas {
    id: canvas

    property bool isSet: false
    property bool highlited: false
    property color regular: "magenta"
    property color highlite: Qt.darker(regular, 1.2)

    contextType: "2d"

    onHighlitedChanged: requestPaint()
    onRegularChanged: requestPaint()
    onEnabledChanged: requestPaint()

    onPaint: {

        context.reset();
        context.fillStyle = enabled
                ? highlited
                  ? highlite
                  : regular
                : "silver";

        context.moveTo(width / 2, 0);
        context.lineTo(width, height / 2);
        context.lineTo(width / 2, height);
        context.lineTo(0, height / 2);
        context.closePath();
        context.fill();
    }
}
