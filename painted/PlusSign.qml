import QtQuick 2.11
import "."

Canvas {
    id: canvas

    property bool isSet: false
    property bool highlited: false
    property color regular: "white"
    property color highlite: Qt.darker(regular, 1.2)

    contextType: "2d"

    onHighlitedChanged: requestPaint()
    onRegularChanged: requestPaint()
    onEnabledChanged: requestPaint()

    onPaint: {

        context.reset();
        context.fillStyle = enabled
                ? highlited
                  ? highlite
                  : regular
                : "silver";

        context.rect(width * 0.5 - height * 0.05 , height * 0.5 - height * 0.25, height * 0.1, height * 0.5);
        context.fill();

        context.rect(width * 0.5  - height * 0.25, height * 0.5 - height * 0.05, height * 0.5, height * 0.1);
        context.fill();
    }
}
