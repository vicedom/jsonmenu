import QtQuick 2.11
import "."

Canvas {
    id: canvas

    property bool isSet: false
    property bool highlited: false
    property color regular: "gray"
    property color highlite: Qt.darker(regular, 1.2)
    property color disabled: "transparent"

    contextType: "2d"

    onHighlitedChanged: requestPaint()
    onRegularChanged: requestPaint()
    onEnabledChanged: requestPaint()

    onPaint: {

        context.reset();
        context.fillStyle = enabled
                ? highlited
                  ? highlite
                  : regular
                : disabled;

        context.rect(width * 1 / 5, 0, width * 1 / 5, height);
        context.fill();

        context.rect(width * 3 / 5, 0, width * 1 / 5, height);
        context.fill();
    }

}
