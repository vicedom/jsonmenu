import QtQuick 2.11
import "."

Canvas {
    id: canvas

    property int type: -1
    property bool highlited: false
    property color regular: "darkslategray"
    property color highlite: "black"
    property color disabled: "transparent"

    contextType: "2d"

    onHighlitedChanged: requestPaint()
    onTypeChanged: requestPaint()
    onRegularChanged: requestPaint()
    onEnabledChanged: requestPaint()

    onPaint: {

        context.reset();
        context.fillStyle = enabled
                ? highlited
                  ? highlite
                  : regular
                : disabled;

        if (type == 0) {
            return;
        }
        else if (type < 0) {

            context.moveTo(0, 0);
            context.lineTo(width, 0);
            context.lineTo(width / 2, height);
            context.closePath();
            context.fill();
        }
        else {

            context.moveTo(width / 2, 0);
            context.lineTo(width, height);
            context.lineTo(0, height);
            context.closePath();
            context.fill();
        }
    }
}
