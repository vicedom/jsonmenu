import QtQuick 2.11
import "."

Canvas {
    id: canvas

    property bool isSet: false
    property bool highlited: false
    property color regular: "gray"
    property color highlite: Qt.darker(regular, 1.2)
    property color disabled: "transparent"

    contextType: "2d"

    onHighlitedChanged: requestPaint()
    onRegularChanged: requestPaint()
    onEnabledChanged: requestPaint()

    onPaint: {
        function draw(xOff)
        {
            context.moveTo(xOff * width, height / 2);
            context.lineTo((xOff + 8 / 30) * width, (4 / 30) * height);
            context.lineTo((xOff + 12 / 30) * width, (4 / 30) * height);
            context.lineTo((xOff + 6 / 30) * width, height / 2);
            context.lineTo((xOff + 12 / 30) * width, (26 / 30) * height);
            context.lineTo((xOff + 8 / 30) * width, (26 / 30) * height);
            context.closePath();
        }

        context.reset();
        context.fillStyle = enabled
                ? highlited
                  ? highlite
                  : regular
                : disabled;

        draw(4 / 30);
        context.fill();
        draw(13 / 30);
        context.fill();
    }
}
