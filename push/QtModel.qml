pragma Singleton
import QtQuick 2.11
import de.techdrivers.bernina 1.0
import "."

QtObject {
    id: model
    objectName: "QtModel"

    property var mainAppModel: MainAppModel { }
}
