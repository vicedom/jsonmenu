import QtQuick 2.11
import "."

QtObject {
    id: model
    objectName: "SubAppSettings"

    property var general: General {  }
    property var personal: Personal { }
}
