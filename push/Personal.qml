import QtQuick 2.11
import "../model"
import "."

QtObject {
    id: model
    objectName: "Personal"

    property var opacity: null

    Component.onCompleted: {
        CppHolder.loader.initModels(model, "personal", "global_model");
    }
}
