import QtQuick 2.11
import "../model"
import "."

QtObject {
    id: model
    objectName: "General"

    property var soundmachine: null
    property var sounds: null
    property var cuifvaluestyles: null
    property var lineinputstyles: null
    property var bernina_cloud_users: null

    property var style_select: null

    property var user_bernina_cloud_at: null
    property var user_bernina_cloud_select: null

    Component.onCompleted: {

        CppHolder.loader.initModels(model, "general", "global_model");
        CppHolder.globalModelBase = this;
    }

}
