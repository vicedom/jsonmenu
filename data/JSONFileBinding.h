#ifndef JSONFILEBINDING_H
#define JSONFILEBINDING_H

#include <QQuickItem>
#include <QTimerEvent>
#include <QVariant>
//#include <QFileSystemWatcher>
#include <QMetaType>

class JSONFileDoc;
class JSONContainer;
class VariantListModel;
class VariantValueModel;
class FileChangeWatcher;

class JSONFileBinding : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool isSet READ isSet WRITE setIsSet NOTIFY isSetChanged)
    Q_PROPERTY(QVariant value READ value WRITE setValue NOTIFY valueChanged)

public:
    JSONFileBinding();
    ~JSONFileBinding() override;

    bool isSet() const;
    void setIsSet(bool isSet);

    QVariant value() const;
    void setValue(const QVariant& valueContent);

    bool connectToFile(const QString& get, const QString& source, FileChangeWatcher* watcher, int refresh = 0);

    bool listModelContentEqual(const QVariantList& listToCheck);
    bool valueModelContentEqual(const QVariantMap& mapToCheck);

    QObject* model();

protected :
    void timerEvent(QTimerEvent* ev) override;

signals:
    void valueChanged();
    void isSetChanged();

private:
    void doValueChanged();

private slots:
    void onListModelContentChanged();
    void onValueModelContentChanged();
    void onJsonFileChanged(const QString& jsonPath);
    void modelDestroyed(QObject* obj);

private:
    QString m_fileSubPath;
    QString m_totalFilePath;
    bool m_isSet;
    QVariant m_jsonValue;
    QVariant m_jsonValueContent;
    QVariant m_orgJsonValue;
    VariantListModel* m_listModel;
    VariantValueModel* m_valueModel;

    int m_refresh;
    int m_tid;
    bool m_isList;
    bool m_isMap;
    QString m_dataSubDir;
    FileChangeWatcher* m_watcher;
};

Q_DECLARE_METATYPE(JSONFileBinding*)

#endif // JSONFILEBINDING_H
