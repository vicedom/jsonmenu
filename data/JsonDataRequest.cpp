#include "JsonDataRequest.h"
#include "JSONFileDoc.h"
#include "JSONContainer.h"

#include <QDebug>

JsonDataRequest::JsonDataRequest(QObject* parent)
    : QObject(parent)
    , m_jsoner(nullptr)
{    
    m_jsoner = new JSONFileDoc(this);
}

JsonDataRequest::~JsonDataRequest()
{

}

QString JsonDataRequest::jsonDataFilePath(const QString& path) const
{
    return m_jsoner->jsonFilePath("/data/" + path);
}

QVariant JsonDataRequest::requestObject(const QString& path)
{
    JSONContainer* res = m_jsoner->read("/data/" + path);
    QVariant result;

    if (res->type() == JSONContainer::ERROR) {
        qDebug() << res->data().toString();
    }
    else if (res->type() != JSONContainer::OBJECT) {
        qDebug() << "Invalid data requesttype <" << path << "> is not an object";
    }
    else {
        result = res->data();
    }

    delete res;
    return result;
}

QVariant JsonDataRequest::requestArray(const QString& path)
{
    JSONContainer* res = m_jsoner->read("/data/" + path);
    QVariant result;

    if (res->type() == JSONContainer::ERROR) {
        qDebug() << res->data().toString();
    }
    else if (res->type() != JSONContainer::ARRAY) {
        qDebug() << "Invalid data requesttype <" << path << "> is not an array";
    }
    else {
        result = res->data();
    }

    delete res;
    return result;
}

JSONContainer* JsonDataRequest::requestContainer(const QString& path)
{
    return m_jsoner->read("/data/" + path);
}
