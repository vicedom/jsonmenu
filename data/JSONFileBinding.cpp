#include "JSONFileBinding.h"
#include "JSONContainer.h"
#include "JSONFileDoc.h"
#include "VariantListModel.h"
#include "VariantValueModel.h"
#include "FileChangeWatcher.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>
#include <QDebug>


JSONFileBinding::JSONFileBinding()
    : m_isSet(false)
    , m_listModel(nullptr)
    , m_valueModel(nullptr)
    , m_refresh(0)
    , m_tid(0)
    , m_isList(false)
    , m_isMap(false)
    , m_dataSubDir("/data/")
    , m_watcher(nullptr)
{

}

JSONFileBinding::~JSONFileBinding()
{
    if (m_tid) {
        killTimer(m_tid);
    }

    if (m_watcher) {
        m_watcher->removeFile(m_totalFilePath, this);
    }

    qDebug() << "d'tor JSONFileBinding::~JSONFileBinding <" << objectName() << "> called";
}

bool JSONFileBinding::isSet() const
{
    return m_isSet;
}

void JSONFileBinding::setIsSet(bool isSet)
{
    if (isSet == m_isSet) {
        return;
    }

    m_isSet = isSet;
    emit isSetChanged();
}

QVariant JSONFileBinding::value() const
{
    return m_jsonValueContent;
}

void JSONFileBinding::setValue(const QVariant& valueContent)
{
    if (!m_isList && !m_isMap) {

        if (valueContent == m_jsonValueContent) {
            return;
        }

        m_jsonValueContent = valueContent;

        QVariant varCopy = m_jsonValue;
        QVariantMap copyMap = varCopy.value<QVariantMap>();
        copyMap["value"] = valueContent;

        m_jsonValue.setValue<QVariantMap>(copyMap);
        doValueChanged();
        emit valueChanged();
    }
}

bool JSONFileBinding::connectToFile(const QString& get, const QString& source, FileChangeWatcher* watcher, int refresh)
{
    if (get.trimmed().isEmpty()) {

        qDebug() << "JSONFileBinding: can not connect to empty file name";
        return false;
    }

    m_watcher = watcher;

    QString path = m_dataSubDir + source;
    path += source.isEmpty() ? "" : "/";
    path += get;

    JSONContainer* res = JSONFileDoc::read(path);
    QVariant result;

    if (res->type() == JSONContainer::ERROR) {

        qDebug() << res->data().toString() << __FILE__ << ": " << __LINE__;
        delete res;
        return false;
    }
    else if (res->type() != JSONContainer::OBJECT) {

        qDebug() << "Invalid data type for JSONFileBinding: <" << path << "> is not an object";
        delete res;
        return false;
    }
    else {
        result = res->data();
        delete res;
    }

    QString totalPath = JSONFileDoc::jsonFilePath(path);

    if (m_watcher) {

        if (refresh > 0) {

            if (!m_watcher->addFile(totalPath, this)) {

                qDebug() << "Could not add <" << path << "> to file watching -> using timer";

                m_watcher = nullptr;
            }
            else {
                connect(m_watcher, &FileChangeWatcher::fileChanged, this, &JSONFileBinding::onJsonFileChanged);
            }
        }
    }

    if (!m_watcher && refresh > 0) {

        m_tid = startTimer(refresh);

        if (!m_tid) {

            qDebug() << "JSONFileBinding <" << path << ">: failed initialize timer";
            return false;
        }
    }        

    QVariantMap valueMap = result.value<QVariantMap>();

    m_isList = false;
    m_isMap = false;
    QVariant::Type type = valueMap.value("value").type();
    bool hasRoles = valueMap.contains("roles");
    bool isList = type == static_cast<QVariant::Type>(QMetaType::QVariantList) && hasRoles;
    bool isMap = type == static_cast<QVariant::Type>(QMetaType::QVariantMap);

    if (!isList && !isMap) {

        m_fileSubPath = path;
        m_totalFilePath = totalPath;
        m_refresh = refresh;
        m_orgJsonValue = result;
        m_jsonValueContent = valueMap.value("value");
        m_jsonValue = result;

        setObjectName(m_fileSubPath);
    }
    else if (isList) {

        m_isList = true;
        m_fileSubPath = path;
        m_totalFilePath = totalPath;
        m_refresh = refresh;

        QVariant varRoles = valueMap.value("roles");
        QVariantList rolesList = varRoles.value<QVariantList>();
        int i, len = rolesList.length();

        QHash<int, QByteArray> rolesHash;
        int k = Qt::UserRole;

        for (i = 0; i < len; i++, k++) {
            rolesHash[k] = rolesList.at(i).toString().toLatin1();
        }

        QVariant varData = valueMap.value("value");
        QVariantList dataList = varData.value<QVariantList>();

        VariantListModel* model = new VariantListModel;

        model->setRoleNames(rolesHash);
        model->setDataPool(dataList);
        model->setProperty("jsonRoles", varRoles);
        model->setObjectName(m_fileSubPath);

        m_listModel = model;
        connect(m_listModel, &VariantListModel::contentChanged, this, &JSONFileBinding::onListModelContentChanged);
        connect(m_listModel, &VariantListModel::destroyed, this, &JSONFileBinding::modelDestroyed, Qt::DirectConnection);

        m_orgJsonValue = result;
        m_jsonValue = result;
        m_jsonValueContent.setValue<VariantListModel*>(model);

        setObjectName(m_fileSubPath);
    }
    else {

        m_isMap = true;
        m_fileSubPath = path;
        m_totalFilePath = totalPath;
        m_refresh = refresh;

        QVariant varData = valueMap.value("value");
        QVariantMap dataMap = varData.value<QVariantMap>();

        VariantValueModel* model = new VariantValueModel;
        model->setDataPool(dataMap);
        model->setObjectName(m_fileSubPath);

        m_valueModel = model;
        connect(m_valueModel, &VariantValueModel::contentChanged, this, &JSONFileBinding::onValueModelContentChanged);
        connect(m_valueModel, &VariantValueModel::destroyed, this, &JSONFileBinding::modelDestroyed, Qt::DirectConnection);

        m_orgJsonValue = result;
        m_jsonValue = result;
        m_jsonValueContent.setValue<VariantValueModel*>(model);

        setObjectName(m_fileSubPath);
    }

    emit valueChanged();

    qDebug() << "JSONFileBinding::connectToFile: <" << m_fileSubPath << ">";

    return true;
}

void JSONFileBinding::modelDestroyed(QObject* obj)
{
    Q_UNUSED(obj);

    if (m_tid) {
        killTimer(m_tid);
    }

    if (m_watcher) {
        m_watcher->removeFile(m_totalFilePath, this);
    }

    m_tid = 0;
    m_watcher = nullptr;

    deleteLater();
}

QObject* JSONFileBinding::model()
{
    if (m_isList) {
        return m_listModel;
    }
    else if (m_isMap) {
        return m_valueModel;
    }

    return nullptr;
}

void JSONFileBinding::doValueChanged()
{
    if (!m_isList && !m_isMap) {

        QJsonDocument doc = QJsonDocument::fromVariant(m_jsonValue);
        QByteArray output = doc.toJson(QJsonDocument::Indented);
        QString path = JSONFileDoc::jsonFilePath(m_fileSubPath);

        QFile file(path);

        if (file.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Unbuffered | QIODevice::Text)) {

            file.write(output);
            file.close();
        }
        else {
            qDebug() << "JSONFileBinding: could not open for write: <" << m_fileSubPath << ">";
        }
    }
}

bool JSONFileBinding::listModelContentEqual(const QVariantList& listToWrite)
{
    JSONContainer* res = JSONFileDoc::read(m_fileSubPath);
    QVariant result;

    if (res->type() == JSONContainer::ERROR) {

        qDebug() << res->data().toString() << __FILE__ << ": " << __LINE__;

        delete res;
        return true;
    }
    else if (res->type() != JSONContainer::OBJECT) {

        qDebug() << "Invalid data type for JSONFileBinding: <" << m_fileSubPath << "> is not an object";

        delete res;
        return false;
    }
    else {
        result = res->data();
        delete res;
    }

    QVariant varValue = result.value<QVariantMap>().value("value");
    QVariantList valueList = varValue.value<QVariantList>();

    return valueList == listToWrite;
}

bool JSONFileBinding::valueModelContentEqual(const QVariantMap& mapToCheck)
{
    JSONContainer* res = JSONFileDoc::read(m_fileSubPath);
    QVariant result;

    if (res->type() == JSONContainer::ERROR) {

        qDebug() << res->data().toString() << __FILE__ << ": " << __LINE__;

        delete res;
        return true;
    }
    else if (res->type() != JSONContainer::OBJECT) {

        qDebug() << "Invalid data type for JSONFileBinding: <" << m_fileSubPath << "> is not an object";

        delete res;
        return false;
    }
    else {
        result = res->data();
        delete res;
    }

    QVariant varValue = result.value<QVariantMap>().value("value");
    QVariantMap valueMap = varValue.value<QVariantMap>();

    return valueMap == mapToCheck;
}

void JSONFileBinding::onListModelContentChanged()
{
    if (listModelContentEqual(m_listModel->poolData())) {
        return;
    }

    QVariant varRoles = m_listModel->property("jsonRoles");
    QVariant varValue;
    varValue.setValue<QVariantList>(m_listModel->poolData());

    QVariantMap outMap;
    outMap["roles"] = varRoles;
    outMap["value"] = varValue;
    QVariant varOut;
    varOut.setValue<QVariantMap>(outMap);

    QJsonDocument doc = QJsonDocument::fromVariant(varOut);
    QByteArray output = doc.toJson(QJsonDocument::Indented);
    QString path = JSONFileDoc::jsonFilePath(m_fileSubPath);

    QFile file(path);

    if (file.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Unbuffered | QIODevice::Text)) {

        file.write(output);
        file.close();
    }
    else {
        qDebug() << "JSONFileBinding: could not open for write: <" << m_fileSubPath << ">";
    }
}

void JSONFileBinding::onValueModelContentChanged()
{
    QVariantMap poolMap = m_valueModel->poolData();

    QVariant varValue;
    varValue.setValue<QVariantMap>(poolMap);

    QVariantMap outMap;
    outMap["value"] = varValue;
    QVariant varOut;
    varOut.setValue<QVariantMap>(outMap);

    QJsonDocument doc = QJsonDocument::fromVariant(varOut);
    QByteArray output = doc.toJson(QJsonDocument::Indented);
    QString path = JSONFileDoc::jsonFilePath(m_fileSubPath);

    QFile file(path);

    if (file.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Unbuffered | QIODevice::Text)) {

        file.write(output);
        file.close();
    }
    else {
        qDebug() << "JSONFileBinding: could not open for write: <" << m_fileSubPath << ">";
    }
}

void JSONFileBinding::onJsonFileChanged(const QString& jsonPath)
{
    if (jsonPath != m_totalFilePath) {
        return;
    }

    timerEvent(nullptr);
}

void JSONFileBinding::timerEvent(QTimerEvent* ev)
{
    static bool updateBlocked = false;

    if (updateBlocked || (ev && ev->timerId() != m_tid)) {
        return;
    }

    updateBlocked = true;

    JSONContainer* res = JSONFileDoc::read(m_fileSubPath);
    QVariant result;

    if (res->type() == JSONContainer::ERROR) {

        qDebug() << res->data().toString() << __FILE__ << ": " << __LINE__;

        delete res;
        updateBlocked = false;
        return;
    }
    else if (res->type() != JSONContainer::OBJECT) {

        qDebug() << "Invalid data type for JSONFileBinding: <" << m_fileSubPath << "> is not an object";

        delete res;
        updateBlocked = false;
        return;
    }
    else {
        result = res->data();
        delete res;
    }

    if (!m_isList && !m_isMap) {

        if (result == m_jsonValue) {

            updateBlocked = false;
            return;
        }

        m_jsonValueContent = result.value<QVariantMap>().value("value");
        m_jsonValue = result;

        emit valueChanged();
    }
    else if (m_isList) {

        QVariant varValue = result.value<QVariantMap>().value("value");
        QVariantList valueList = varValue.value<QVariantList>();

        const QVariantList& dataList = m_listModel->poolData();

        if (valueList == dataList) {

            updateBlocked = false;
            return;
        }

        m_listModel->mergeDataPool(valueList);
    }
    else {

        QVariant varValue = result.value<QVariantMap>().value("value");
        QVariantMap valueMap = varValue.value<QVariantMap>();

        QVariantMap dataMap = m_valueModel->poolData();

        if (valueMap == dataMap) {

            updateBlocked = false;
            return;
        }

        m_valueModel->mergeDataPool(valueMap);
    }

    updateBlocked = false;
}





