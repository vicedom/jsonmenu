#ifndef JSONDATAREQUEST_H
#define JSONDATAREQUEST_H

#include <QObject>
#include <QVariant>

class JSONFileDoc;
class JSONContainer;

class JsonDataRequest : public QObject
{
    Q_OBJECT
public:
    explicit JsonDataRequest(QObject* parent = nullptr);
    ~JsonDataRequest() override;

    QVariant requestObject(const QString& path);
    QVariant requestArray(const QString& path);
    JSONContainer* requestContainer(const QString& path);

    QString jsonDataFilePath(const QString& path) const;

private:
    JSONFileDoc* m_jsoner;
};

#endif // JSONDATAREQUEST_H
