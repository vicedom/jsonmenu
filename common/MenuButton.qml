import QtQuick 2.11
import QtQuick.Controls 2.5
import "../painted"
import "."

RadioButton {
    id: control
    objectName: "MenuButton"

    signal executeCommand(var from, var what, var args)

    property var clicko: null

    property real fontSize: 13
    property color regular: "gray"
    property color highlite: Qt.darker(regular, 1.2)
    property var paintee: null

    opacity: enabled ? 1.0 : 0.8
    clip: true
    activeFocusOnTab: true
    enabled: {

        if (clicko) {

            if (clicko.command) {
                return clicko.command.length > 0;
            }
        }

        return false;
    }

    property bool pressedAndHeld: false    

    onPainteeChanged: {

        if (paintee && !paintee.isSet) {

            paintee.isSet = true;
            paintee.highlited = Qt.binding(function() { return control.down });
            paintee.anchors.verticalCenter = Qt.binding(function() { return control.verticalCenter });
            paintee.anchors.left = Qt.binding(function() { return control.left });
            paintee.anchors.leftMargin = 8;
            paintee.visible = Qt.binding(function() { return control.checked });
            paintee.height = Qt.binding(function() { return control.fontSize * 1.2 });
            paintee.width = Qt.binding(function() { return paintee.visible ? paintee.height * 0.6 : 0 });
            control.indicator = paintee;
        }
    }

    contentItem: Text {
        anchors.verticalCenter: control.indicator.verticalCenter
        text: control.text
        font.pixelSize: fontSize
        leftPadding: control.indicator ? control.indicator.width + control.spacing + 5 : 0
        color: enabled
                ? control.down
                   ? control.checked
                     ? Qt.darker(highlite, 1.2)
                     : highlite
                   : control.checked
                     ? Qt.lighter(regular, 1.2)
                     : regular
                : "whitesmoke"
        verticalAlignment: Text.AlignVCenter
        width: control.width
        wrapMode: Text.WordWrap
        opacity: enabled ? 1.0 : 0.6
    }

    onPressAndHold: pressedAndHeld = true

    onReleased: {

        if (clicko && pressedAndHeld) {

            pressedAndHeld = false;
            checked = true;
            executeCommand(control, clicko, null);
        }
    }

    onClicked: {

        if (clicko) {
            executeCommand(control, clicko, null);
        }
    }
}
