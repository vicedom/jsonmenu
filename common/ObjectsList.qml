import QtQuick 2.11
import QtQuick.Controls 2.5
import QtQml.Models 2.11
import "."

ListView {
    id: control
    objectName: "ObjectsList"

    signal executeCommand(var from, var what, var args)

    property bool isSet: false
    property bool fixit: false
    property real popupHeight: height
    property var population: null

    onPopulationChanged: {

        if (population) {

            var i;
            var len = population.length;
            var object;

            model.clear();

            for (i = 0; i < len; i++) {

                object = population[i];

                if (object.executeCommand) {

                    object.width = Qt.binding(function() { return control.width });
                    if (object.popupHeight) {
                        object.popupHeight = Qt.binding(function() { return control.popupHeight });
                    }

                    object.executeCommand.connect(executeCommand);
                    model.append(object);
                }
            }
        }
    }

    clip: true
    activeFocusOnTab: true
    snapMode: ListView.SnapToItem
    boundsMovement: fixit ? Flickable.StopAtBounds : Flickable.FollowBoundsBehavior

    model: ObjectModel { }

    ScrollIndicator.vertical: ScrollIndicator { }

    function delegateAt(idx)
    {
        return model.get(idx);
    }
}
