import QtQuick 2.11
import QtGraphicalEffects 1.0
import "../scripts/propter.js" as Propter
import "."

FocusRect {
    id: control
    objectName: "InputLine.qml"

    property var at: null
    property var lineSet: null

    onLineSetChanged: {
        Propter.applyTextProps(labelTxt, lineSet.label.textDisplay);
        Propter.applyTextProps(inputInp, lineSet.input.textDisplay);
    }

    activeFocusOnTab: true

    function executeSelf()
    {
        if (at) {
            at.input = inputInp.text;
        }
    }

    Rectangle {
        id: labelRct

        anchors {
            top: parent.top
            bottom: parent.bottom
            left: parent.left
        }
        width: parent.width / 2

        color: "transparent"
        clip: true

        Text {
            id: labelTxt

            anchors {
                fill: parent
                topMargin: 4
                bottomMargin: 4
                leftMargin: 12
                rightMargin: 12
            }

            elide: Text.ElideRight
            clip: true
            verticalAlignment: Text.AlignVCenter
            text: (at) ? at.label : ""
        }
    }

    FocusRect {
        id: inputRct

        anchors {
            top: parent.top
            bottom: parent.bottom
            right: parent.right
        }
        width: parent.width / 2

        color: lineSet.input.background
        clip: true
        activeFocusOnTab: true

        TextInput {
            id: inputInp

            anchors {
                fill: parent
                topMargin: 4
                bottomMargin: 4
                leftMargin: 12
                rightMargin: eyeRct.width + 12
            }
            clip: true
            verticalAlignment: TextInput.AlignVCenter
            echoMode: (at && at.password)
                      ? eyeMousi.pressed
                        ? TextInput.Normal
                        : TextInput.Password
                      : TextInput.Normal
            selectByMouse: true
            text: (at)
                  ? at.password
                    ? ""
                    :  at.input
                  : ""
        }

        FocusRect {
            id: eyeRct

            anchors {
                top: parent.top
                bottom: parent.bottom
                right: parent.right
            }
            width: visible ? height : 0
            visible: at && at.password

            color: inputRct.color

            Image {
                id: eyeImg

                anchors.fill: parent

                source: "qrc:/image/general/EyeNormal.png"
                fillMode: Image.PreserveAspectCrop
            }

            MouseArea {
                id: eyeMousi

                anchors.fill: parent
            }

            ColorOverlay {
                anchors.fill: eyeImg
                source: eyeImg
                color: "#80808080"
                visible: eyeMousi.pressed
            }
        }
    }
}
