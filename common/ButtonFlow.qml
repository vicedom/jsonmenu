import QtQuick 2.11
import QtQuick.Controls 2.5
import "."

Flow {
    id: container
    objectName: "ButtonFlow"

    signal executeCommand(var from, var what, var args)

    property var population: null

    onPopulationChanged: {

        if (population) {

            var i;
            var len = population.length;
            var object;

            for (i = 0; i < len; i++) {

                object = population[i];
                object.executeCommand.connect(executeCommand);
            }
        }
    }

    focus: true
}
