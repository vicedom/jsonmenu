import QtQuick 2.11
import "."

FocusRect {
    id: control
    objectName: "PaintedButton"

    signal executeCommand(var from, var what, var args)

    property var clicko: null
    property var paintee: null

    activeFocusOnTab: true
    enabled: (clicko && clicko.command) ? clicko.command.length > 0 : false
    color: "transparent"

    property bool pressedAndHeld: false

    onPainteeChanged: {

        if (paintee && !paintee.isSet) {

            paintee.isSet = true;
            paintee.highlited = Qt.binding(function() { return mousi.pressed });
            paintee.anchors.fill = control;
        }
    }

    MouseArea {
        id: mousi
        anchors.fill: parent

        onPressAndHold: pressedAndHeld = true

        onReleased: {

            if (clicko && pressedAndHeld) {

                pressedAndHeld = false;
                executeCommand(control, clicko, null);
            }
        }

        onClicked: {

            if (clicko) {
                executeCommand(control, clicko, null);
            }
        }
    }
}
