import QtQuick 2.11
import QtGraphicalEffects 1.0
import QtQuick.Controls 2.5
import "../scripts/propter.js" as Propter
import "."

ItemDelegate {
    id: control
    objectName: "LinkButton"

    signal executeCommand(var from, var what, var args)

    property bool isSet: false
    property var clicko: null
    property var textDisplay: null
    property var msg: null
    property color visited: "lightskyblue"
    property color regular: "lightskyblue"

    onTextDisplayChanged: {

        if (textDisplay) {

            if (textDisplay.font && textDisplay.font.pixelSize) {
                linkTxt.font.pixelSize = textDisplay.font.pixelSize;
            }

            if (textDisplay.font && textDisplay.font.bold) {
                linkTxt.font.bold = textDisplay.font.bold;
            }

            if (textDisplay.text) {
                linkTxt.text = textDisplay.text;
            }

            if (textDisplay.color) {
                regular = textDisplay.color;
            }
        }
    }

    width: 5 //linkTxt.paintedWidth + 12
    height: 30
    activeFocusOnTab: true
    enabled: (clicko && clicko.command && clicko.command.length > 0) ? true : false
    focus: enabled

    property bool pressedAndHeld: false
    property bool hasBeenClicked: false

    contentItem: Text {

        id: linkTxt

        anchors {
            top: parent.top
            bottom: parent.bottom
            left: parent.left
        }
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideNone

        color: enabled
               ? hasBeenClicked
                 ? visited
                 : regular
               : "silver"

        onTextChanged: {
            control.width = paintedWidth + 12
        }
    }

    background: Rectangle {
        implicitWidth: 100
        implicitHeight: 40
        color: "transparent"
    }

    onPressAndHold: pressedAndHeld = true

    onReleased: {

        if (clicko && pressedAndHeld) {

            pressedAndHeld = false;
            executeCommand(control, clicko, msg);
            hasBeenClicked = true;
        }
    }

    onClicked: {

        if (clicko) {

            executeCommand(control, clicko, msg);
            hasBeenClicked = true;
        }
    }
}
