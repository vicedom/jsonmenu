import QtQuick 2.11
import "../painted"
import "../scripts/propter.js" as Propter
import "."

FocusRect {
    id: control
    objectName: "CUIFValueSelect"

    property string icon: ""
    property var at: null
    property var valueSet: null

    onValueSetChanged: Propter.applyTextProps(valueTxt, valueSet.textDisplay)

    width: valueSetRct.width + plusminusRct.width + minusBtn.anchors.rightMargin * 2
    color:  "transparent"

    FocusRect {
        id: valueSetRct

        anchors {
            left: parent.left
            top: parent.top
            bottom: parent.bottom
        }
        width: iconImg.width + valueTxt.width

        color: valueSetMousi.pressed ? Qt.darker(valueSet.regular, 1.2) : valueSet.regular
        radius: valueSet.radius

        Image {
            id: iconImg

            anchors {
                top: parent.top
                bottom: parent.bottom
                left: parent.left
            }
            width: (icon.length > 0) ? height : 0
            source: icon
            verticalAlignment: Image.AlignVCenter
            horizontalAlignment: Image.AlignHCenter
            fillMode: Image.PreserveAspectCrop
        }

        Text {
            id: valueTxt

            anchors {

                top: parent.top
                bottom: parent.bottom
                left: iconImg.right
            }
            width: height * 1.5

            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: (at) ? at.value.toFixed(valueSet.fixed) : ""

        }

        MouseArea {
            id: valueSetMousi

            anchors.fill: parent

            onClicked: {

                if (at) {
                    at.value = at.defaultValue
                }
            }
        }
    }

    FocusRect {
        id: plusminusRct

        anchors {
            right: parent.right
            top: parent.top
            bottom: parent.bottom
        }
        width: minusBtn.width + minusBtn.anchors.rightMargin + plusBtn.width

        color: "transparent"

        FocusRect {
            id: minusBtn

            anchors {
                right: plusBtn.left
                rightMargin: 5
                top: parent.top
                bottom: parent.bottom
            }
            width: height

            color: minusMousi.pressed ? Qt.darker(valueSet.plusminus.regular, 1.2) : valueSet.plusminus.regular
            radius: valueSet.plusminus.radius

            MinusSign {
                anchors.fill: parent
                regular: valueSet.plusminus.painted.regular
                highlite: valueSet.plusminus.painted.highlite
            }

            MouseArea {
                id: minusMousi

                anchors.fill: parent

                onClicked: {

                    if (at) {

                        var step = valueSet.plusminus.step

                        if ((at.value - step / 100) >= at.minValue) {
                            at.value -= step / 100;
                        }
                        else {
                            at.value = at.minValue;
                        }
                    }
                }
            }
        }

        FocusRect {
            id: plusBtn

            anchors {
                right: parent.right
                top: parent.top
                bottom: parent.bottom
            }
            width: height

            color: plusMousi.pressed ? Qt.darker(valueSet.plusminus.regular, 1.2) : valueSet.plusminus.regular
            radius: minusBtn.radius

            PlusSign {
                anchors.fill: parent
                regular: valueSet.plusminus.painted.regular
                highlite: valueSet.plusminus.painted.highlite
            }

            MouseArea {
                id: plusMousi

                anchors.fill: parent

                onClicked: {

                    if (at) {

                        var step = valueSet.plusminus.step

                        if ((at.value + step / 100) <= at.maxValue) {
                            at.value += step / 100;
                        }
                        else {
                            at.value = at.maxValue;
                        }
                    }
                }
            }
        }
    }
}
