import QtQuick 2.11
import QtQuick.Controls 2.5
import "../scripts/propter.js" as Propter
import "."

FocusRect {
    id: control
    objectName: "UpDownSlider.qml"

    property var at: null
    property var valueSlide: null

    onValueSlideChanged: Propter.applyTextProps(toValueTxt, valueSlide.textDisplay)

    Slider {
        id: slider

        anchors {
            top: parent.top
            topMargin: 15
            bottom: parent.bottom
            bottomMargin: 15
            horizontalCenter: parent.horizontalCenter
        }

        width: 25
        orientation: Qt.Vertical

        from: (at) ? at.minValue * 100 : 0
        to: (at) ? at.maxValue * 100 : 100
        value: (at) ? at.value * 100 : 100

        onMoved: {

            if (at) {
                at.value = value / 100;
            }
        }
    }

    Text {
        id: toValueTxt

        anchors {
            top: parent.top
            right: slider.left
            rightMargin: 15
        }

        height: 30

        verticalAlignment: Text.AlignBottom
        text: slider.to.toFixed(valueSlide.fixed)
    }

    Text {
        id: fromValueTxt

        anchors {
            bottom: parent.bottom
            right: slider.left
            rightMargin: 15
        }

        height: 30

        verticalAlignment: Text.AlignTop
        text: slider.from.toFixed(valueSlide.fixed)

        color: toValueTxt.color
        font: toValueTxt.font
    }
}
