import QtQuick 2.11
import QtQuick.Controls 2.5
import QtQml.Models 2.11
import "../painted"
import "."

ComboBox {
    id: control
    objectName: "TextCombo"

    signal executeCommand(var from, var what, var args)

    property bool isSet: false
    property var at: null
    property var population: null

    onPopulationChanged: {

        var test;

        if (population) {

            test = population;

            if (population.length) {

                model.clear();
                var len = population.length;
                var i;

                for (i = 0; i < len; i++) {
                    model.append(population[i]);
                }
            }
            else {
                delegateModel.model = population;
            }
        }
    }

    onAtChanged: {

        var test;

        if (at) {
            test = at;
        }
    }

    onActivated: {

        if (at) {
            at.idx = index;
        }
    }

    function currentValue()
    {
        if (currentIndex < 0) {
            return "";
        }

        return delegateModel.model.get(currentIndex).value;
    }

    property real popupHeight: 500

    currentIndex: (at) ? at.idx : -1
    textRole: "key"
    displayText: "  " + currentText
    model: ListModel { }


    delegate: ItemDelegate {

        width: control.width - 3
        height: control.height
        x: 1
        contentItem: Text {
            text: key
            color: model.color
            font.pixelSize: 16
            elide: Text.ElideRight
            verticalAlignment: Text.AlignVCenter
        }
        highlighted: control.highlightedIndex === index

        onClicked: {
            model.color = "lightskyblue";
        }
    }

    indicator: TurnRight {

        x: control.width - width - 5
        y: control.topPadding + (control.availableHeight - height) / 2        
        height: 20
        width: height * 1.2
        regular: "darkslategray"
        highlited: control.pressed
        opacity: 0.8
    }

    contentItem: Text {

        leftPadding: 0
        rightPadding: control.indicator.width + control.spacing

        text: control.displayText
        font.pixelSize: 16
        color: control.pressed ? "black" : "darkslategray"
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight
    }

    background: Rectangle {

        implicitWidth: 120
        implicitHeight: 40
        border {
            color: control.pressed ? "black" : "darkslategray"
            width: control.visualFocus ? 2 : 1
        }
    }

    popup: Popup {

        anchors.centerIn: Overlay.overlay
        width: control.width - 2
        implicitHeight: contentItem.implicitHeight + 2
        padding: 1
        modal: true

        contentItem: ListView {

            clip: true
            implicitHeight: contentHeight <= popupHeight ? contentHeight : popupHeight
            model: control.popup.visible ? control.delegateModel : null
            currentIndex: control.highlightedIndex

            ScrollIndicator.vertical: ScrollIndicator { }
        }

        background: Rectangle {

            border.color: "darkslategrey"
        }

        // @disable-check M16
        Overlay.modal: Rectangle {
            color: "#aacfdbe7"
        }
    }

}
