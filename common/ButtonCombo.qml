import QtQuick 2.11
import QtQuick.Controls 2.5
import QtQml.Models 2.11
import "."

ComboBox {
    id: control
    objectName: "ButtonCombo"

    signal executeCommand(var from, var what, var args)

    property bool isSet: false
    property string icon: ""
    property var population: null

    onPopulationChanged: {

        if (population) {

            if (population.length) {

                model.clear();
                var len = population.length;
                var i;

                for (i = 0; i < len; i++) {
                    model.append(population[i]);
                }
            }
            else {
                delegateModel.model = population;
            }
        }
    }

    model: ListModel { }

    delegate: ItemDelegate {
        width: control.width
        height: control.height
        x: 0
        contentItem: Rectangle {

            color: enabled ? "transparent" : "whitesmoke"
            enabled: (clicko && clicko.command) ? clicko.command.length > 0 : false

            Image {
                anchors.fill: parent

                verticalAlignment: Image.AlignVCenter
                horizontalAlignment: Image.AlignHCenter
                fillMode: Image.PreserveAspectCrop
                source: from
                opacity: parent.enabled ? 1.0 : 0.6
            }
        }
        highlighted: false
        onClicked: executeCommand(control, clicko, null)
    }

    indicator: Image {
        height: control.height
        width: control.width
        verticalAlignment: Image.AlignVCenter
        horizontalAlignment: Image.AlignHCenter
        fillMode: Image.PreserveAspectCrop
        source: control.icon
    }

    background: Rectangle {
        implicitWidth: 70
        implicitHeight: 70
        color: "whitesmoke"
    }

    popup: Popup {
        y: control.height
        x: 0
        width: control.width
        implicitHeight: contentItem.implicitHeight
        padding: 0

        contentItem: ListView {
            clip: true
            implicitHeight: contentHeight
            model: control.popup.visible ? control.delegateModel : null
            currentIndex: control.highlightedIndex
        }

        background: Rectangle {
            border.color: "transparent"
        }
    }
}
