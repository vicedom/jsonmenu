#include "FileChangeWatcher.h"

#include <QStringList>

#include <QDebug>

FileChangeWatcher::FileChangeWatcher(QObject* parent)
    : QObject(parent)
    , m_paused(false)
{
    m_timer.setInterval(300);
    connect(&m_timer, SIGNAL(timeout()), this, SLOT(checkFiles()));
}

FileChangeWatcher::~FileChangeWatcher()
{

}

void FileChangeWatcher::setRefresh(int val)
{
    if (val == refresh() || val < 50) {
        return;
    }

    m_timer.setInterval(val);
    emit refreshChanged();
}

void FileChangeWatcher::setPaused(bool val)
{
    if (val == m_paused) {
        return;
    }

    m_paused = val;
    emit pausedChanged();
}

bool FileChangeWatcher::addFile(const QString& file, QObject* obj)
{
    if (!obj || !QFileInfo::exists(file)) {
        return false;
    }

    bool wasEmpty = m_path_info_hash.isEmpty();

    QFileInfo fileInfo(file);
    QDateTime dt = fileInfo.lastModified();

    QString canonicalFilePath = fileInfo.canonicalFilePath();

    if (!m_path_info_hash.contains(canonicalFilePath)) {

        fileInfo.setCaching(false);
        info_p info(fileInfo, dt.toMSecsSinceEpoch());

        m_path_info_hash[canonicalFilePath] = info;
    }

    if (!m_path_user_hash.contains(canonicalFilePath)) {

        object_l list;
        m_path_user_hash[canonicalFilePath] = list;
    }

    if (!m_path_user_hash[canonicalFilePath].contains(obj)) {

        connect(obj, &QObject::destroyed, this, &FileChangeWatcher::userDestroyed, Qt::DirectConnection);
        m_path_user_hash[canonicalFilePath].append(obj);
    }

    qDebug() << "FileChangeWatcher::addFile: <" << canonicalFilePath << ">";

    emit fileAdded(canonicalFilePath);

    if (wasEmpty) {
        m_timer.start();
    }

    return true;
}

bool FileChangeWatcher::removeFile(const QString& file, QObject* obj)
{
    if (!obj) {
        return false;
    }

    QFileInfo fileInfo(file);
    QString canonicalFilePath = fileInfo.canonicalFilePath();

    if (!m_path_info_hash.contains(canonicalFilePath) ||
        !m_path_user_hash.contains(canonicalFilePath)) {
        return false;
    }

    if (!m_path_user_hash[canonicalFilePath].contains(obj)) {
        return false;
    }

    qDebug() << "FileChangeWatcher::removeFile: <" << canonicalFilePath << ">";

    m_path_user_hash[canonicalFilePath].takeAt(m_path_user_hash[canonicalFilePath].indexOf(obj));

    if (m_path_user_hash[canonicalFilePath].isEmpty()) {

        m_path_info_hash.remove(canonicalFilePath);
        m_path_user_hash.remove(canonicalFilePath);
    }

    emit fileRemoved(canonicalFilePath);

    if (m_path_info_hash.isEmpty()) {
        m_timer.stop();
    }

    return true;
}

void FileChangeWatcher::userDestroyed(QObject* obj)
{
    if (!obj) {
        return;
    }

    QStringList files;

    path_user_h::const_iterator it = m_path_user_hash.constBegin();

    while (it != m_path_user_hash.constEnd()) {

        if (it.value().contains(obj)) {
            files.append(it.key());
        }

        ++it;
    }

    int i, len = files.length();

    for (i = 0; i < len; i++) {
        removeFile(files.at(i), obj);
    }
}

void FileChangeWatcher::checkFiles()
{
    static bool checkInProgress = false;

    if (checkInProgress || m_paused) {
        return;
    }

    checkInProgress = true;

    path_info_h::iterator it = m_path_info_hash.begin();
    QString from;

    while (it != m_path_info_hash.end()) {

        qint64 dtWas = it.value().second;
        qint64 dtIs = it.value().first.lastModified().toMSecsSinceEpoch();

        if (dtWas != dtIs) {

            from = it.key();

            it.value().second = dtIs;
            emit fileChanged(it.key());
        }

        ++it;
    }

    checkInProgress = false;
}
