#ifndef FILECHANGEWATCHER_H
#define FILECHANGEWATCHER_H

#include <QObject>
#include <QDateTime>
#include <QFileInfo>
#include <QPair>
#include <QHash>
#include <QTimer>

class FileChangeWatcher : public QObject
{
    typedef QPair<QFileInfo, qint64> info_p;
    typedef QHash<QString, info_p> path_info_h;
    typedef QList<QObject*> object_l;
    typedef QHash<QString, object_l> path_user_h;

    Q_OBJECT

    Q_PROPERTY(int refresh READ refresh WRITE setRefresh NOTIFY refreshChanged)
    Q_PROPERTY(bool paused READ paused WRITE setPaused NOTIFY pausedChanged)

public:
    explicit FileChangeWatcher(QObject* parent = nullptr);
    ~FileChangeWatcher();

    int refresh() const
    {
        return m_timer.interval();
    }
    void setRefresh(int val);

    bool paused() const
    {
        return m_paused;
    }
    void setPaused(bool val);

    bool addFile(const QString& file, QObject* obj);
    bool removeFile(const QString& file, QObject* obj);

signals:
    void fileChanged(const QString& file);
    void fileAdded(const QString& file);
    void fileRemoved(const QString& file);

    void refreshChanged();
    void pausedChanged();

private slots:
    void userDestroyed(QObject* obj);
    void checkFiles();

private:
    path_info_h m_path_info_hash;
    path_user_h m_path_user_hash;

    QTimer m_timer;
    bool m_paused;
};

#endif // FILECHANGEWATCHER_H
