import QtQuick 2.11
import QtQuick.Window 2.11
import QtQuick.Controls 2.5
import "./common"
import "."

Window {
    id: window
    visible: true
    width: 1280
    height: 800
    title: qsTr("Json Menu")

    Popup {
        id: popolino

        property var inlay: null

        anchors.centerIn: Overlay.overlay

        modal: true
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside

        // @disable-check M16
        Overlay.modal: Rectangle {
            color: "#aacfdbe7"
        }

        onInlayChanged: {

            if (inlay) {

                contentHeight = inlay.implicitHeight;
                contentWidth = inlay.implicitWidth;
                inlay.closeRequest.connect(close);
                contentItem = inlay;
                open();
            }
        }

        onClosed: {

            if (inlay) {

                inlay.destroy();
                inlay = null;
            }
        }
    }

    FocusRect {
        id: root
        objectName: "root"

        anchors.fill: parent

        opacity: QtModel.mainAppModel.subAppSettings.personal.opacity.value

        readonly property var patholino: patholinoRect
        readonly property var menulino: menulinoRect
        readonly property var contento: contentoRect

        FocusRect {
            id: patholinoRect
            objectName: "patholino"

            property var inlay: null

            anchors {
                top: parent.top
                left: parent.left
            }
            height: 50
            width: 250

            color: "darkmagenta";

            property var lastInlay: null

            onInlayChanged: {

                if (inlay) {

                    if (lastInlay) {

                        lastInlay.visible = false;
                        lastInlay.destroy();
                        lastInlay = null;
                    }

                    inlay.anchors.fill = patholinoRect;
                    inlay.executeCommand.connect(executeCommand);
                    lastInlay = inlay;
                }
            }
        }

        FocusRect {
            id: menulinoRect
            objectName: "menulino"

            property var inlay: null

            anchors {
                top: patholinoRect.bottom
                left: parent.left
                bottom: parent.bottom
            }
            width: patholinoRect.width
            color: patholinoRect.color

            property var lastInlay: null

            onInlayChanged: {

                if (inlay) {

                    if (lastInlay) {

                        lastInlay.visible = false;
                        lastInlay.destroy();
                        lastInlay = null;
                    }

                    inlay.anchors.fill = menulinoRect;
                    inlay.anchors.leftMargin = 50;
                    inlay.executeCommand.connect(executeCommand);
                    lastInlay = inlay;
                }
            }
        }

        FocusRect {
            id: contentoRect
            objectName: "contento"

            property var inlay: null

            anchors {
                top: parent.top
                left: menulinoRect.right
                right: parent.right
                bottom: parent.bottom
            }

            color: "magenta"
            activeFocusOnTab: true

            property var lastInlay: null

            onInlayChanged: {

                if (inlay) {

                    if (lastInlay) {

                        lastInlay.visible = false;
                        lastInlay.destroy();
                        lastInlay = null;
                    }

                    inlay.anchors.fill = contentoRect;
                    inlay.executeCommand.connect(executeCommand);
                    lastInlay = inlay;
                }
            }
        }
    }

    function executeCommand(from, what, args)
    {
        CppHolder.loader.initContents(what.command, what.source, what.use, args);
    }

    Component.onCompleted: {

        CppHolder.loader.initContainer(root);
        CppHolder.loader.registerPopup(popolino);
    }
}
