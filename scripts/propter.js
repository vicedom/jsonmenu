function applyTextProps(txt, prop)
{
    if (!prop) {
        return;
    }

    if (prop.font && prop.font.pixelSize) {
        txt.font.pixelSize = prop.font.pixelSize;
    }

    if (prop.font && prop.font.bold) {
        txt.font.bold = prop.font.bold;
    }

    if (prop.color) {
        txt.color = prop.color;
    }

    if (prop.text) {
        txt.text = prop.text;
    }
}
