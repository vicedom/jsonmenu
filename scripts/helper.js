var _epsilon = 0.00001

function qmlFuzzyCompare(r1, r2)
{
    return (Math.abs(r1 - r2) <= _epsilon * Math.min(Math.abs(r1), Math.abs(r2)));
}

function qmlFuzzyIsNull(r)
{
    return Math.abs(r) <= _epsilon;
}
