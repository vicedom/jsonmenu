#ifndef JSONFILEDOCUMENT_H
#define JSONFILEDOCUMENT_H

#include "JSONContainer.h"

class JSONFileDoc : public QObject
{
    Q_OBJECT

public:
    JSONFileDoc(QObject* parent = nullptr);
    ~JSONFileDoc() override;

    static void setJsonRoot(const QString& val);

    static JSONContainer* read(const QString& fileName);
    static QString jsonFilePath(const QString& fileName);

private:
    static QString* m_jsonRoot;

};

#endif // JSONFILEDOCUMENT_H
