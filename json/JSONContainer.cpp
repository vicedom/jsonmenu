#include "JSONContainer.h"

JSONContainer::JSONContainer(QObject* parent)
    : QObject(parent)
    , m_dataType(UNDEFINED)
{

}

JSONContainer::~JSONContainer()
{

}

QVariant JSONContainer::data() const
{
    return m_data;
}

void JSONContainer::setData(const QVariant& val)
{
    m_data = val;
    emit dataChanged();
}

JSONContainer::DataType JSONContainer::type() const
{
    return m_dataType;
}

void JSONContainer::setType(DataType val)
{
    if (val == m_dataType) {
        return;
    }

    m_dataType = val;
    emit typeChanged();
}




















