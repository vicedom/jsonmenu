#include "JSONFileDoc.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonParseError>
#include <QFile>
#include <QByteArray>

#include <QDebug>

QString* JSONFileDoc::m_jsonRoot = nullptr;
void JSONFileDoc::setJsonRoot(const QString& val)
{
    if (!m_jsonRoot) {

        m_jsonRoot = new QString;
        *m_jsonRoot = val;
    }
}

JSONFileDoc::JSONFileDoc(QObject* parent)
    : QObject(parent)
{

}

JSONFileDoc::~JSONFileDoc()
{
    delete m_jsonRoot;
    m_jsonRoot = nullptr;
}

JSONContainer* JSONFileDoc::read(const QString& fileName)
{
    JSONContainer* result = new JSONContainer;

    QFile file(*m_jsonRoot + fileName + ".json");
    QByteArray json;

    if (file.open(QIODevice::ReadOnly | QIODevice::Unbuffered)) {

        json = file.readAll();
        file.close();
    }

    QJsonParseError pe;
    QJsonDocument doc = QJsonDocument::fromJson(json, &pe);

    if (pe.error) {

        QString output = QString(tr("Error reading/parsing json file '%1': %2 (%3)")).arg(*m_jsonRoot + fileName).arg(pe.errorString()).arg(pe.error);

        result->setType(JSONContainer::ERROR);
        QVariant val(output);
        result->setData(val);
    }
    else {

        result->setType(doc.isArray() ? JSONContainer::ARRAY : JSONContainer::OBJECT);
        result->setData(doc.toVariant());
    }

    return result;
}

QString JSONFileDoc::jsonFilePath(const QString& fileName)
{
    return *m_jsonRoot + fileName + ".json";
}

























