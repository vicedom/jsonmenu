#ifndef JSONCONTAINER_H
#define JSONCONTAINER_H

#include <QObject>
#include <QVariant>
#include <QMetaType>

class JSONContainer : public QObject
{
    Q_OBJECT
    Q_ENUMS(DataType)

    Q_PROPERTY(QVariant data READ data WRITE setData NOTIFY dataChanged)
    Q_PROPERTY(DataType type READ type WRITE setType NOTIFY typeChanged)

public:
    enum DataType
    {
        UNDEFINED,
        OBJECT,
        ARRAY,
        ERROR,
    };

public:
    explicit JSONContainer(QObject* parent = nullptr);
    ~JSONContainer() override;

    QVariant data() const;
    void setData(const QVariant& val);

    DataType type() const;
    void setType(DataType val);

Q_SIGNALS:
    void dataChanged();
    void typeChanged();

private:
    DataType m_dataType;
    QVariant m_data;
};

Q_DECLARE_METATYPE(JSONContainer*)

#endif // JSONCONTAINER_H
