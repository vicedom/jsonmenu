import QtQuick 2.11
import QtQuick.Controls 2.5
import QtQml.Models 2.11
import "../common"
import "../painted"
import "../scripts/propter.js" as Propter
import "."

FocusRect {
    id: container
    objectName: "ListFlash"

    signal executeCommand(var from, var what, var args)

    property color btnColor: "mediumspringgreen"

    property var title: null
    property var explain: null
    property var globnav: null
    property var objects: null

    onTitleChanged: Propter.applyTextProps(titleTxt, title)
    onExplainChanged: Propter.applyTextProps(explainTxt, explain)

    onGlobnavChanged: {

        if (globnav && !globnav.isSet) {

            globnav.isSet = true;
            globnav.z = z + 1
            globnav.anchors.fill = Qt.binding(function() { return globnavRct });
            globnav.executeCommand.connect(executeCommand);
        }
    }

    onObjectsChanged: {

        if (objects && !objects.isSet) {

            objects.isSet = true;
            objects.anchors.fill = Qt.binding(function() { return objectsRct });
            if (objects.popupHeight) {
                objects.popupHeight = Qt.binding(function() { return height - 50 });
            }
            if (objects.executeCommand) {
                objects.executeCommand.connect(executeCommand);
            }
        }
    }

    activeFocusOnTab: true

    function proceedObjects()
    {
        if (!objects) {
            return;
        }

        var i;
        var delg;
        var len = objects.count;

        for (i = len - 1; i >= 0; i--) {

            delg = objects.delegateAt(i);

            if (!delg.executeSelf) {
                continue;
            }

            delg.executeSelf();
        }

        for (i = len - 1; i >= 0; i--) {

            delg = objects.delegateAt(i);

            if (!delg.executeResult) {
                continue;
            }

            delg.executeResult();
        }
    }

    Text {
        id: titleTxt

        anchors {
            left: parent.left
            leftMargin: 50
            top: parent.top
            topMargin: 20
            right: globnavRct.left
            rightMargin: 10
        }
        verticalAlignment: Text.AlignVCenter
    }

    Text {
        id: explainTxt

        anchors {
            left: titleTxt.left
            top: titleTxt.bottom
            topMargin: 10
            right: parent.right
            rightMargin: 50
        }
        verticalAlignment: Text.AlignVCenter
    }

    Rectangle {
        id: globnavRct

        anchors {
            top: parent.top
            right: parent.right
        }

        height: 70
        width: 70
    }

    Rectangle {
        id: objectsRct

        anchors {
            top: explainTxt.bottom
            topMargin: 30
            left: explainTxt.left
            right: explainTxt.right
            bottom: parent.bottom
            bottomMargin: 100
        }
        color: "transparent"
    }

    FocusRect {
        id: executeRct

        anchors {
            right: parent.right
            rightMargin: 20
            bottom: parent.bottom
            bottomMargin: 20
        }
        height: 60
        width: height * 2.5

        color: executeMousi.pressed ? Qt.darker(btnColor, 1.2) : btnColor
        clip: true

        Image {

            anchors {
                centerIn: parent
            }

            width: sourceSize.width
            height: sourceSize.height
            source: "qrc:/image/general/CheckNormal.png"
            verticalAlignment: Image.AlignVCenter
            horizontalAlignment: Image.AlignHCenter
            fillMode: Image.PreserveAspectCrop
        }

        MouseArea {
            id: executeMousi

            anchors.fill: parent

            onClicked: proceedObjects()
        }
    }
}

