import QtQuick 2.11
import "."

Rectangle {
    id: control
    objectName: "PictureSplash"

    signal executeCommand(var from, var what, var args)

    property var pictures: null
    property var at: null
    property double fill: 0.3333

    Rectangle {
        id: imageRect

        anchors.centerIn: parent
        width: Math.min(parent.width,  parent.width * fill);
        height: Math.min(parent.height, parent.height * fill);
        color:  ani.status != Image.Ready ? "ghostwhite" : "transparent"

        AnimatedImage {
            id: ani
            anchors.fill: parent

            asynchronous: true
            fillMode: Image.PreserveAspectFit
            source: (pictures && at) ? pictures[at.value].from : ""

            onStatusChanged: playing = (status == Image.Ready)
        }
    }

    Component.onDestruction: {

        console.log("PictureSplash destructed <", objectName, ">");
    }
}
