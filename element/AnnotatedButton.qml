import QtQuick 2.11
import "../common"
import "."

FocusRect {
    id: control
    objectName: "AnnotatedButton"

    color: "transparent"

    signal executeCommand(var from, var what, var args)

    property var button: null
    property real fontSize: 13
    property string label: ""
    property color labelColor: "black"
    property real labelOffset: 5

    activeFocusOnTab: true

    onButtonChanged: {

        if (button && !button.isSet) {

            button.isSet = true;
            button.height = Qt.binding(function() { return height * 2 / 3 });
            button.width = Qt.binding(function() { return height });
            button.anchors.verticalCenter = Qt.binding(function() { return verticalCenter });
            button.x = Qt.binding(function() { return x + labelOffset / 3 });
            button.executeCommand.connect(executeCommand);
            labeler.x = Qt.binding(function() { return x + height });
        }
    }    

    Text {
        id: labeler
        anchors.verticalCenter: parent.verticalCenter

        font.pixelSize: fontSize
        verticalAlignment: Text.AlignVCenter
        leftPadding: labelOffset
        color: labelColor
        text: label
        x: parent.x
    }    
}
