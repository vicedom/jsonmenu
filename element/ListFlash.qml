import QtQuick 2.11
import QtQuick.Controls 2.5
import QtQml.Models 2.11
import "../common"
import "../painted"
import "../scripts/propter.js" as Propter
import "."

FocusRect {
    id: container
    objectName: "ListFlash"

    signal executeCommand(var from, var what, var args)

    property var title: null
    property var explain: null
    property var globnav: null
    property var objects: null

    onTitleChanged: Propter.applyTextProps(titleTxt, title)
    onExplainChanged: Propter.applyTextProps(explainTxt, explain)

    onGlobnavChanged: {

        if (globnav && !globnav.isSet) {

            globnav.isSet = true;
            globnav.z = z + 1
            globnav.anchors.fill = Qt.binding(function() { return globnavRct });
            globnav.executeCommand.connect(executeCommand);
        }
    }

    onObjectsChanged: {

        if (objects && !objects.isSet) {

            objects.isSet = true;            
            objects.anchors.fill = Qt.binding(function() { return objectsRct });
            if (objects.popupHeight) {
                objects.popupHeight = Qt.binding(function() { return height - 50 });
            }
        }
    }

    activeFocusOnTab: true

    Text {
        id: titleTxt

        anchors {
            left: parent.left
            leftMargin: 50
            top: parent.top
            topMargin: 20
            right: globnavRct.left
            rightMargin: 10
        }
        verticalAlignment: Text.AlignVCenter
    }

    Text {
        id: explainTxt

        anchors {
            left: titleTxt.left
            top: titleTxt.bottom
            topMargin: 10
            right: parent.right
            rightMargin: 50
        }
        verticalAlignment: Text.AlignVCenter
    }

    Rectangle {
        id: globnavRct

        anchors {
            top: parent.top
            right: parent.right
        }

        height: 70
        width: 70
    }

    Rectangle {
        id: objectsRct

        anchors {
            top: explainTxt.bottom
            topMargin: 30
            left: explainTxt.left
            right: explainTxt.right
            bottom: parent.bottom
            bottomMargin: 15
        }
        color: "transparent"
    }
}
