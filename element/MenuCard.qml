import QtQuick 2.11
import QtQuick.Controls 2.5
import QtQml.Models 2.11
import "../common"
import "../painted"
import "."

FocusRect {
    id: container
    objectName: "MenuCard"

    signal executeCommand(var from, var what, var args)

    property var population: null
    property var entry: null

    clip: true
    opacity: enabled ? 1.0 : 0.6

    onPopulationChanged: {

        if (population) {

            listModel.clear();
            var len = population.length;
            var i;

            for (i = 0; i < len; i++) {
                listModel.append(population[i]);
            }
        }
    }

    ListModel {
        id: listModel
    }

    ListView {
        id: listView

        anchors.fill: parent
        model: listModel

        delegate: Component {

            MenuButton {

                width: listView.width

                paintee: Diamond {
                    regular: "lavender"
                }

                clicko: model.clicko
                fontSize: model.fontSize ? model.fontSize : entry.fontSize ? entry.fontSize : 15
                regular: model.regular ? model.regular : entry.regular ? entry.regular : "forestgreen"
                objectName: model.objectName
                text: model.text

                Component.onCompleted: {
                    executeCommand.connect(container.executeCommand)
                }
            }
        }
    }
}
