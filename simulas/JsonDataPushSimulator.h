#ifndef JSONDATAPUSHSIMULATOR_H
#define JSONDATAPUSHSIMULATOR_H

#include <QQuickItem>
#include <QHash>
#include <QPair>
#include <QTimerEvent>

namespace simulas {

typedef struct _path_id_max_tid
{
    QString path;
    QString pushId;
    int max;
    int tid;
} path_id_max_tid;

typedef QHash<QObject*, path_id_max_tid> hash_objectstar_path_id_max_tid;

class JsonDataPushSimulator : public QObject
{
    Q_OBJECT

public:
    explicit JsonDataPushSimulator(QObject* parent = nullptr);
    ~JsonDataPushSimulator() override;

    void registerArrayIndexItem(QObject* item, const QString& path, const QString& pushId, int max, int wait);

protected :
    void timerEvent(QTimerEvent* ev) override;

signals:
    void updateModel(const QString& pushId);

private slots:
    void destroyWatchee(QObject* obj);

private:
    hash_objectstar_path_id_max_tid m_arrayIndexItems;

    static QString m_arrayIndexFileContent;
};

} // namespace

#endif // JSONDATAPUSHSIMULATOR_H
