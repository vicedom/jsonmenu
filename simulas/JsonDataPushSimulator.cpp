#include "JsonDataPushSimulator.h"

#include <QRandomGenerator>
#include <QFile>

#include <QDebug>

using namespace simulas;

QString JsonDataPushSimulator::m_arrayIndexFileContent =
        "{\n"
        "   \"value\": %1\n"
        "}\n";

JsonDataPushSimulator::JsonDataPushSimulator(QObject* parent)
    : QObject(parent)
{

}

JsonDataPushSimulator::~JsonDataPushSimulator()
{

}

void JsonDataPushSimulator::registerArrayIndexItem(QObject* item, const QString& path, const QString& pushId, int max, int wait)
{
    if (!item || path.isEmpty() || pushId.isEmpty() || max < 2 || wait < 100) {
        return;
    }

    int tid = startTimer(wait);

    if (!tid) {
        return;
    }

    path_id_max_tid pimt;
    pimt.path = path;
    pimt.pushId = pushId;
    pimt.max = max;
    pimt.tid = tid;

    if (connect(item, SIGNAL(destroyed(QObject*)), this, SLOT(destroyWatchee(QObject*)), Qt::DirectConnection)) {
        m_arrayIndexItems.insertMulti(item, pimt);
    }
}

void JsonDataPushSimulator::timerEvent(QTimerEvent* ev)
{
    hash_objectstar_path_id_max_tid::const_iterator it = m_arrayIndexItems.constBegin();

    while (it != m_arrayIndexItems.constEnd()) {

        path_id_max_tid pimt = it.value();

        if (pimt.tid == ev->timerId()) {

            int idx = QRandomGenerator::global()->bounded(0, pimt.max);
            QString toWrite = m_arrayIndexFileContent.arg(idx);

            QFile file(pimt.path);

            if (file.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Unbuffered | QIODevice::Text)) {

                file.write(toWrite.toUtf8());
                file.close();

                emit updateModel(pimt.pushId);
            }
        }

        ++it;
    }
}

void JsonDataPushSimulator::destroyWatchee(QObject* obj)
{
    hash_objectstar_path_id_max_tid::const_iterator it = m_arrayIndexItems.constBegin();

    while (it != m_arrayIndexItems.constEnd()) {

        if (it.key() == obj) {
            killTimer(it.value().tid);
        }

        ++it;
    }

    m_arrayIndexItems.remove(obj);
}
