#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "CppLoader.h"
#include "JSONFileDoc.h"
#include "VariantListModel.h"
#include "VariantValueModel.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    qRegisterMetaType<VariantListModel*>("VariantListModel*");
    qRegisterMetaType<VariantValueModel*>("VariantValueModel*");
    JSONFileDoc::setJsonRoot("C:/ProjekteTest/JsonMenu/simulation");
    qmlRegisterType<CppLoader>("de.techdrivers.bernina", 1, 0, "CppLoader");

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
