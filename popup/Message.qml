import QtQuick 2.11
import "../common"
import "../scripts/propter.js" as Propter
import "."

FocusRect {
    id: contentItem
    objectName: "Message"

    signal closeRequest

    property int iconIndex: -1
    property var msg: null

    implicitHeight: titleTxtRct.height + divider1Rct.height + message1TxtRct.height +
                    message2TxtRct.height + fillerRct.height  + divider2Rct.height + buttonOkAreaRct.height + 35
    implicitWidth: Math.max(buttonOkRct.width, Math.max(titleTxtRct.width, message1TxtRct.width, message2TxtRct.width) + messageIcn.width) + 25

    onMsgChanged: {

        if (msg) {

            iconIndex = msg.iconIndex;
            Propter.applyTextProps(titleTxt, msg.title);
            Propter.applyTextProps(message1Txt, msg.message1);
            Propter.applyTextProps(message2Txt, msg.message2);

            switch (iconIndex) {

                default: messageIcn.source = ""; break;
                case 0: messageIcn.source = "qrc:/image/general/Info.png"; break;
                case 1: messageIcn.source = "qrc:/image/general/Question.png"; break;
                case 2: messageIcn.source = "qrc:/image/general/Exclamat.png"; break;
                case 3: messageIcn.source = "qrc:/image/general/No.png"; break;
                case 4: messageIcn.source = "qrc:/image/general/Empty.png"; break;
            }
        }
    }

    property bool pressedAndHeld: false

    anchors {
        centerIn: parent
    }

    Rectangle {
        id: titleTxtRct

        anchors {
            top: parent.top
            topMargin: 4
            left: parent.left
            leftMargin: 4
        }
        height: 10
        width:10

        color: "transparent"

        Text {
            id: titleTxt

            anchors {
                verticalCenter: parent.verticalCenter
                left: parent.left
            }
            verticalAlignment: Text.AlignVCenter
            clip: true
            elide: Text.ElideNone

            onTextChanged: {

                parent.height = paintedHeight + 8;
                parent.width = paintedWidth + 8;
            }
        }
    }

    Rectangle {
        id: divider1Rct

        anchors {
            top: titleTxtRct.bottom
            topMargin: 4
            left: titleTxtRct.left
            right: parent.right
            rightMargin: 4
        }
        height: 1

        color: "silver"
    }

    Image {
        id: messageIcn

        anchors {
            top: divider1Rct.bottom
            topMargin: 20
            left: divider1Rct.left
        }
        height: 32
        width: sourceSize.width > 0 ? height : 0
        fillMode: Image.PreserveAspectFit
    }

    Rectangle {
        id: message1TxtRct

        anchors {
            top: divider1Rct.bottom
            topMargin: 20
            left: messageIcn.right
            leftMargin: 14
        }
        height: 10
        width: 10

        color: "transparent"

        Text {
            id: message1Txt

            anchors {
                verticalCenter: parent.verticalCenter
                left: parent.left
            }
            verticalAlignment: Text.AlignVCenter
            clip: true
            elide: Text.ElideNone

            onTextChanged: {

                parent.height = paintedHeight + 8;
                parent.width = paintedWidth + 8;
            }
        }
    }

    Rectangle {
        id: message2TxtRct

        anchors {
            top: message1TxtRct.bottom
            topMargin: 4
            left: message1TxtRct.left
        }
        height: 10
        width: 20

        color: "transparent"

        Text {
            id: message2Txt

            anchors {
                verticalCenter: parent.verticalCenter
                left: parent.left
            }
            verticalAlignment: Text.AlignVCenter
            clip: true
            elide: Text.ElideNone

            onTextChanged: {

                parent.height = paintedHeight + 8;
                parent.width = paintedWidth + 8;
            }
        }
    }

    Rectangle {
        id: fillerRct

        anchors {
            top: message2TxtRct.bottom
            left: message2TxtRct.left
        }
        height: 8
        width: 20

        color: "transparent"
    }

    Rectangle {
        id: divider2Rct

        anchors {
            top: fillerRct.bottom
            topMargin: 4
            left: titleTxtRct.left
            right: parent.right
            rightMargin: 4
        }
        height: 1

        color: "silver"
    }

    FocusRect {
        id: buttonOkAreaRct

        anchors {
            top: divider2Rct.bottom
            topMargin: 4
            left: titleTxtRct.left
            right: parent.right
            rightMargin: 4
        }
        height: 80

        color: "transparent"

        FocusRect {
            id: buttonOkRct

            anchors {
                centerIn: parent
            }
            height: 60
            width: 150

            property color regular: "springgreen"

            color: okMousi.pressed ? Qt.darker(regular, 1.2) : regular

            Text {
                id: buttonOkTxt

                anchors {
                    centerIn: parent
                }

                font {
                    pixelSize: 28
                    bold: true
                }

                property color regular: "darkslategray"

                color: okMousi.pressed ? Qt.darker(regular, 1.2) : regular
                text: "OK"
            }

            MouseArea {
                id: okMousi

                anchors {
                    fill: parent
                }

                onPressAndHold: pressedAndHeld = true

                onReleased: {

                    if (pressedAndHeld) {

                        pressedAndHeld = false;
                        closeRequest();
                    }
                }

                onClicked: closeRequest()
            }
        }
    }

    function setArgs(args)
    {
        msg = args;
    }

    Component.onDestruction: {
        console.log("Message destructed <", objectName, ">");
    }
}
