QT += quick qml network

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += json
INCLUDEPATH += content
INCLUDEPATH += data
INCLUDEPATH += simulas
INCLUDEPATH += model
INCLUDEPATH += file

SOURCES += \
        content/CppLoader.cpp \
        content/JsonContentRequest.cpp \
        data/JSONFileBinding.cpp \
        data/JsonDataRequest.cpp \
        file/FileChangeWatcher.cpp \
        json/JSONContainer.cpp \
        json/JSONFileDoc.cpp \
        main.cpp \
        model/VariantListModel.cpp \
        model/VariantValueModel.cpp \
        simulas/JsonDataPushSimulator.cpp

RESOURCES += qml.qrc \
    resource.qrc \
    simulation/provided/provided.qrc

HEADERS += \
    content/CppLoader.h \
    content/JsonContentRequest.h \
    data/JSONFileBinding.h \
    data/JsonDataRequest.h \
    file/FileChangeWatcher.h \
    json/JSONContainer.h \
    json/JSONFileDoc.h \
    model/VariantListModel.h \
    model/VariantValueModel.h \
    simulas/JsonDataPushSimulator.h

MOC_DIR     = generated
OBJECTS_DIR = generated
UI_DIR      = generated
RCC_DIR     = generated

DISTFILES += \
    simulation/content/io.json \
    simulation/content/machine/middle_left.json \
    simulation/content/machine/right.json \
    simulation/content/machine/sound.json \
    simulation/content/machine/top_left.json \
    simulation/content/personal/middle_left.json \
    simulation/content/personal/opacity.json \
    simulation/content/personal/right.json \
    simulation/content/personal/top_left.json \
    simulation/content/pop.json \
    simulation/content/popup/message.json \
    simulation/content/root.json \
    simulation/content/machine/root.json \
    simulation/content/settings/middle_left.json \
    simulation/content/settings/right.json \
    simulation/content/settings/top_left.json \
    simulation/content/wifi/middle_left.json \
    simulation/content/wifi/my_bernina_cloud.json \
    simulation/content/wifi/right.json \
    simulation/content/wifi/top_left.json \
    simulation/data/general/bernina_cloud_users.json \
    simulation/data/general/cuifvaluestyles.json \
    simulation/data/general/global_model.json \
    simulation/data/general/globnav.json \
    simulation/data/general/lineinputstyles.json \
    simulation/data/general/sounds.json \
    simulation/data/machine/back.json \
    simulation/data/machine/construction.json \
    simulation/data/machine/menu.json \
    simulation/data/machine/sounds_list.json \
    simulation/data/personal/back.json \
    simulation/data/personal/construction.json \
    simulation/data/personal/global_model.json \
    simulation/data/personal/menu.json \
    simulation/data/personal/opacity_list.json \
    simulation/data/settings/back.json \
    simulation/data/settings/construction.json \
    simulation/data/settings/menu.json \
    simulation/data/volatile/global_opacity_val.json \
    simulation/data/volatile/global_soundmachine_at.json \
    simulation/data/volatile/global_style_select.json \
    simulation/data/volatile/global_user_bernina_cloud_at.json \
    simulation/data/volatile/global_user_bernina_cloud_select.json \
    simulation/data/volatile/machine_construction_at.json \
    simulation/data/volatile/machine_sounds_1_at.json \
    simulation/data/volatile/machine_sounds_2_at.json \
    simulation/data/volatile/machine_sounds_3_at.json \
    simulation/data/volatile/machine_sounds_4_at.json \
    simulation/data/volatile/my_bernina_cloud_forgot_password_message.json \
    simulation/data/volatile/personal_construction_at.json \
    simulation/data/volatile/settings_construction_at.json \
    simulation/data/volatile/wifi_construction_at.json \
    simulation/data/wifi/back.json \
    simulation/data/wifi/construction.json \
    simulation/data/wifi/menu.json \
    simulation/data/wifi/my_bernina_cloud_list.json \
    simulation/data/wifi/my_bernina_cloud_reg_flow.json \
    simulation/data/wifi/my_bernina_cloud_reg_flow_buttons.json
