#ifndef VARIANTLISTMODEL_H
#define VARIANTLISTMODEL_H

#include <QAbstractListModel>
#include <QVariant>
#include <QVector>
#include <QMetaType>

class VariantListModel : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(QVariantList dataPool READ dataPool WRITE setDataPool NOTIFY dataPoolChanged)

    // Qml model
    Q_PROPERTY(int count READ count)

public:
    explicit VariantListModel(QObject* parent = nullptr);
    ~VariantListModel() override;

    QVariantList dataPool() const;
    void setDataPool(const QVariantList& val);
    void mergeDataPool(const QVariantList& val);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

    void setRoleNames(const QHash<int, QByteArray>& val);
    QHash<int, QByteArray> roleNames() const override;

    // Qml model
    int count() const
    {
        return rowCount();
    }
    Q_INVOKABLE QVariant get(int idx) const;
    //Q_INVOKABLE void setProperty(int index, const QString property, const QVariant& value) /*throws*/;

    void insert(int idx, const QVariant& val);
    void append(const QVariant& val);
    void prepend(const QVariant& val);
    bool remove(int idx);
    bool remove(int from, int to);
    bool move(int from, int to);

    void emitContentChanged();

    const QVariantList& poolData() const;

    static int registeredType()
    {
        return m_registeredMetaType;
    }

signals:
    void dataPoolChanged();
    void contentChanged();
    void rolesChanged();

private:
    QVariantList m_dataPool;
    QHash<int, QByteArray> m_roleNames;
    QVector<int> m_allRoles;
    bool m_contentChangeBlocked;

    static int m_registeredMetaType;
};

Q_DECLARE_METATYPE(VariantListModel*)

#endif // VARIANTLISTMODEL_H
