import QtQuick 2.11
import "../common"
import "."

FocusRect {
    id: delegate
    objectName: "InputLineDelegate"

    signal executeCommand(var from, var what, var args)

    property var at: null
    //property string atProp: ""
    property var lineinputstyles: QtModel.mainAppModel.subAppSettings.general.lineinputstyles.value
    property var select: QtModel.mainAppModel.subAppSettings.general.style_select.value

    height: 60
    activeFocusOnTab: true

    function executeSelf()
    {
        inputLine.executeSelf();
    }

    InputLine {
        id: inputLine

        anchors {
            fill: parent
        }

        at: delegate.at
        lineSet: lineinputstyles[select]
    }

    Component.onDestruction: {
        console.log("InputLineDelegate destructed <", objectName, ">");
    }
}
