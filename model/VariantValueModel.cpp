#include "VariantValueModel.h"

#include <QMetaObject>
#include <QMetaMethod>
#include <QMetaProperty>

#include <QDebug>

int VariantValueModel::m_registeredMetaType = 0;

VariantValueModel::VariantValueModel(QObject* parent)
    : QQmlPropertyMap(this, parent)
{
    m_registeredMetaType = qRegisterMetaType<VariantValueModel*>("VariantValueModel*");
    connect(this, &VariantValueModel::valueChanged, this, &VariantValueModel::onValueChanged);
}

VariantValueModel::~VariantValueModel()
{
    qDebug() << "d'tor VariantValueModel::~VariantValueModel <" << objectName() << "> called";
}

void VariantValueModel::onValueChanged(const QString& key, const QVariant& value)
{
    Q_UNUSED(key);
    Q_UNUSED(value);    

    emit contentChanged();
}

void VariantValueModel::setDataPool(const QVariantMap& dataMap)
{
    QVariantMap::const_iterator it = dataMap.constBegin();

    while (it != dataMap.constEnd()) {

        QVariant varValue = it.value();

        if (varValue.isValid()) {

            QString key = it.key();
            QVariant::Type type = varValue.type();

            if (type == static_cast<QVariant::Type>(QMetaType::QVariantMap)) {

                VariantValueModel* model = new VariantValueModel(this);
                QString objName = objectName();
                objName += !objName.isEmpty() ? "/" : "";
                objName += key;
                model->setObjectName(objName);
                connect(model, &VariantValueModel::contentChanged, this, &VariantValueModel::contentChanged);

                QVariantMap valueMap = varValue.value<QVariantMap>();
                model->setDataPool(valueMap);
                varValue.setValue<VariantValueModel*>(model);

                insert(key, varValue);
            }
            else {
                insert(key, varValue);
            }
        }

        ++it;
    }
}

void VariantValueModel::mergeDataPool(const QVariantMap& val)
{
    QVariantMap::const_iterator it = val.constBegin();

    while (it != val.constEnd()) {

        QVariant varValue = it.value();
        QString key = it.key();
        QVariant::Type type = varValue.type();

        QVariant varOwn = value(key);

        if (varOwn.isValid()) {

            if (type == static_cast<QVariant::Type>(QMetaType::QVariantMap)) {

                VariantValueModel* model = varOwn.value<VariantValueModel*>();

                if (model) {

                    QVariantMap contentMap = varValue.value<QVariantMap>();
                    model->mergeDataPool(contentMap);
                }
            }
            else {
                insert(key, varValue);
            }
        }

        ++it;
    }
}

QVariantMap VariantValueModel::poolData() const
{
    QVariantMap resultMap;
    QStringList keysList = keys();
    int i, len = keysList.length();

    for (i = 0; i < len; i++) {

        QString key = keysList.at(i);
        QVariant varValue = value(key);

        if (!varValue.isValid()) {
            continue;
        }

        int type = varValue.type();

        if (type == QVariant::UserType) {
            type = varValue.userType();
        }

        if (type == m_registeredMetaType) {

            VariantValueModel* model = varValue.value<VariantValueModel*>();

            if (!model) {
                continue;
            }

            QVariantMap valueMap = model->poolData();
            varValue.setValue<QVariantMap>(valueMap);
            resultMap[key] = varValue;
        }
        else {
            resultMap[key] = varValue;
        }
    }

    return resultMap;
}

