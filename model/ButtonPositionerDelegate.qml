import QtQuick 2.11
import QtQuick.Controls 2.5
import "../common"
import "."

FocusRect {
    id: delegate
    objectName: "ButtonPositionerDelegate"

    signal executeCommand(var from, var what, var args)

    property real flowSpacing: 5
    property var positioner: null

    onPositionerChanged: {

        if (positioner && !positioner.isSet) {

            positioner.isSet = true;
            var object = positioner[0];

            object.anchors.fill = Qt.binding(function() { return positionerRct });
            object.executeCommand.connect(executeCommand);
            groupi.buttons = Qt.binding(function() { return object.children });

            object.forceLayout();
            height = Qt.binding(function() { return object.height });
        }
    }

    height: 30
    color: "transparent"

    FocusRect {
        id: positionerRct

        anchors {
            fill: parent
        }

        color: "transparent"
    }

    ButtonGroup {
        id: groupi
        exclusive: true;
    }

    Component.onDestruction: {
        console.log("ButtonPositionerDelegate destructed <", objectName, ">");
    }
}
