import QtQuick 2.11
import QtQuick.Controls 2.5
import "../common"
import "../painted"
import "../scripts/propter.js" as Propter
import "."

FocusRect {
    id: delegate
    objectName: "SoundPropsDelegate"

    height: 80

    signal executeCommand(var from, var what, var args)

    property string icon: ""
    property var name: null
    property var loudness: null
    property var speakerIcon: null
    property var at: QtModel.mainAppModel.subAppSettings.general.soundmachine

    onNameChanged: Propter.applyTextProps(nameTxt, name)
    onLoudnessChanged: Propter.applyTextProps(loudnessTxt, loudness)

    Image {
        id: iconImg

        anchors {

            top: parent.top
            topMargin: 5
            bottom: parent.bottom
            bottomMargin: 5
            left: parent.left
            leftMargin: 10
        }
        width: height

        verticalAlignment: Image.AlignVCenter
        horizontalAlignment: Image.AlignHCenter
        fillMode: Image.PreserveAspectFit
        source: icon
    }

    Text {
        id: nameTxt

        anchors {
            left: iconImg.right
            leftMargin: 10
            verticalCenter: parent.verticalCenter
            right: volumeSld.left
            rightMargin: 10
        }
        elide: Text.ElideLeft
        verticalAlignment: Text.AlignVCenter
    }

    Slider {
        id: volumeSld

        anchors {
            horizontalCenter: parent.horizontalCenter
            horizontalCenterOffset: 30
            verticalCenter: parent.verticalCenter
        }
        width: 200
        //live: false

        from: 0
        to: 100
        value: (at) ? at.volume * 100 : 100

        onMoved: {

            if (at) {
                at.volume = value / 100;
            }
        }
    }

    Text {
        id: loudnessTxt

        anchors {
            left: volumeSld.right
            leftMargin: 15
            right: speakerIconImg.left
            rightMargin: 15
            verticalCenter: parent.verticalCenter
        }

        verticalAlignment: Text.AlignVCenter
        text: volumeSld.value.toFixed(0)
    }

    Image {
        id: speakerIconImg

        anchors {
            right: notMutedSwt.left
            rightMargin: 20
            verticalCenter: parent.verticalCenter
        }
        height: 32
        width: height
        fillMode: Image.PreserveAspectFit
        source: (speakerIcon)
                ? notMutedSwt.checked
                  ? speakerIcon.on
                  : speakerIcon.off
                : ""
    }

    Switch {
        id: notMutedSwt

        anchors {
            right: parent.right
            rightMargin: 20
            verticalCenter: parent.verticalCenter
        }

        height: 32
        width: 40

        checkable: true
        checked: (at) ? !at.muted : false

        onCheckedChanged: {

            if (at) {
                at.muted = !checked;
            }
        }
    }

    Component.onDestruction: {
        console.log("SoundPropsDelegate destructed <", objectName, ">");
    }
}
