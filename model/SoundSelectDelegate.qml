import QtQuick 2.11
import QtQuick.Controls 2.5
import QtQml.Models 2.11
import "../common"
import "../painted"
import "../scripts/propter.js" as Propter
import "."

FocusRect {
    id: delegate
    objectName: "SoundSelectDelegate"

    height: 80

    signal executeCommand(var from, var what, var args)

    property string icon: "qrc:/image/general/Empty.png"
    property var name: null
    property var sounds: QtModel.mainAppModel.subAppSettings.general.sounds
    property var at: null

    onNameChanged: Propter.applyTextProps(nameTxt, name)

    property real popupHeight: 500

    color: "transparent"

    Image {
        id: iconImg

        anchors {

            top: parent.top
            topMargin: 5
            bottom: parent.bottom
            bottomMargin: 5
            left: parent.left
            leftMargin: 10
        }
        width: height

        verticalAlignment: Image.AlignVCenter
        horizontalAlignment: Image.AlignHCenter
        fillMode: Image.PreserveAspectFit
        source: icon
    }

    Text {
        id: nameTxt

        anchors {
            left: iconImg.right
            leftMargin: 10
            verticalCenter: parent.verticalCenter
            right: soundCmb.left
            rightMargin: 10
        }
        elide: Text.ElideLeft
        verticalAlignment: Text.AlignVCenter
    }

    TextCombo {
        id: soundCmb

        anchors {
            horizontalCenter: parent.horizontalCenter
            horizontalCenterOffset: 30
            verticalCenter: parent.verticalCenter
        }
        width: 200

        popupHeight: delegate.popupHeight
        population: sounds
        at: delegate.at
    }

    Rectangle {
        id: playBtnCenterer

        anchors {
            left: soundCmb.right
            right: onSwt.left
            verticalCenter: parent.verticalCenter
        }
        height: soundCmb.height
        color: "transparent"

        ToolButton {
            id: playBtn

            anchors.centerIn: parent
            width: parent.width / 3
            height: parent.height - 2
            checkable: true
            checked: false
            background: Rectangle {

                implicitWidth: 100
                implicitHeight: 40
                color: playBtn.checked ? "gainsboro" : "whitesmoke"
                border.width: 1
            }
            enabled: SoundMachine.actConsumer() === null || SoundMachine.actConsumer() === delegate

            LeftRightIndicator {

                anchors {
                    verticalCenter: parent.verticalCenter
                    horizontalCenter: parent.horizontalCenter
                    horizontalCenterOffset: 4
                }
                type: 1
                regular: "darkslategray"
                disabled: "lightgrey"
                highlited: playBtn.down
                width: parent.height * 0.6
                height: width * 1.2
                visible: !playBtn.checked
            }

            PauseIndicator {

                anchors {
                    verticalCenter: parent.verticalCenter
                    horizontalCenter: parent.horizontalCenter
                    horizontalCenterOffset: 0
                }

                regular: "darkslategray"
                disabled: "lightgrey"
                highlited: playBtn.down
                height: parent.height * 0.6
                width: height * 1.2
                visible: playBtn.checked
            }

            onCheckedChanged: playButton(checked)
        }
    }

    Switch {
        id: onSwt

        anchors {
            right: parent.right
            rightMargin: 20
            verticalCenter: parent.verticalCenter
        }

        height: 32
        width: 40

        checkable: true
        checked: (at) ? at.on : false

        onCheckedChanged: {

            if (at) {
                at.on = checked;
            }
        }
    }

    function playButton(play)
    {
        if (!play) {
            SoundMachine.stopPlay();
        }
        else {
            var from = soundCmb.currentValue();

            if (from.length > 0) {
                SoundMachine.playForMe(from, delegate, "playingStopped");
            }
        }
    }

    function playingStopped() {
        playBtn.checked = false;
    }

    Component.onDestruction: {

        SoundMachine.stopPlay();
        console.log("SoundSelectDelegate destructed <", objectName, ">");
    }
}
