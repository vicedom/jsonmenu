import QtQuick 2.11
import "../common"
import "."

Rectangle {
    id: delegate
    objectName: "ExecuteAccessDelegate"

    signal executeCommand(var from, var what, var args)

    property string iconSuccess: ""
    property string iconFail: ""
    property string titleSuccess: ""
    property string titleFail: ""
    property string messageSuccess: ""
    property string messageFail: ""

    property var at: null
    property var accessAt: null
    property var globalUsers: null
    property var globalIndexTo: null

    color: "transparent"

    property int resultState: 0

    states: [
        State {
            name: "SLEEPING"
            when: resultState == 0
            PropertyChanges {
                target: delegate
                height: 0
            }
            PropertyChanges {
                target: failRect
                opacity: 0
            }
            PropertyChanges {
                target: successRect
                opacity: 0
            }
        },
        State {
            name: "SUCCESS"
            when: resultState == 2
            PropertyChanges {
                target: delegate
                height: successRect.height
            }
            PropertyChanges {
                target: successRect
                opacity: 1
            }
        },
        State {
            name: "FAILED"
            when: resultState == 1
            PropertyChanges {
                target: delegate
                height: failRect.height
            }
            PropertyChanges {
                target: failRect
                opacity: 1
            }
        }
    ]

    transitions: Transition {
        PropertyAnimation { properties: "height"; easing.type: Easing.InOutQuad ; duration: 500 }
        PropertyAnimation { target: failRect; properties: "opacity"; easing.type: Easing.InOutQuad ; duration: 500 }
        PropertyAnimation { target: successRect; properties: "opacity"; easing.type: Easing.InOutQuad ; duration: 500 }
    }

    Rectangle {
        id: containerRct

        anchors {
            fill: parent
            topMargin: 1
            bottomMargin: 1
        }

        color: "transparent"

        Rectangle {
            id: successRect

            anchors {
                centerIn: parent
            }

            height: successTitle.anchors.topMargin * 2 + successTitle.paintedHeight + successMessage.anchors.topMargin + successMessage.paintedHeight
            width: successTitle.anchors.leftMargin * 2 + Math.min(delegate.width, Math.max(successTitle.paintedWidth, successMessage.paintedWidth))


            color: "#2200bfff"
            border {
                width: 2
                color: "deepskyblue"
            }
            opacity: 0

            Text {
                id: successTitle

                anchors {

                    top: parent.top
                    topMargin: 15
                    left: parent.left
                    leftMargin: 20
                }

                font {
                    pixelSize: 25
                    bold: true
                }

                color: Qt.darker(parent.border.color, 1.2);
                verticalAlignment: Text.AlignVCenter
                text: titleSuccess
            }

            Text {
                id: successMessage

                anchors {
                    top: successTitle.bottom
                    topMargin: 20
                    left: successTitle.left
                }

                font {
                    pixelSize: 20
                    bold: false
                }

                color: Qt.darker(parent.border.color, 1.2);
                verticalAlignment: Text.AlignVCenter
                text: messageSuccess
            }
        }

        Rectangle {
            id: failRect

            anchors {
                centerIn: parent
            }

            height: failTitle.anchors.topMargin * 2 + failTitle.paintedHeight + failMessage.anchors.topMargin + failMessage.paintedHeight
            width: failTitle.anchors.leftMargin * 2 + Math.min(delegate.width, Math.max(failTitle.paintedWidth, failMessage.paintedWidth))


            color: "#22ff0000"
            border {
                width: 2
                color: "red"
            }            
            opacity: 0

            Text {
                id: failTitle

                anchors {

                    top: parent.top
                    topMargin: 15
                    left: parent.left
                    leftMargin: 20
                }

                font {
                    pixelSize: 25
                    bold: true
                }

                color: Qt.darker(parent.border.color, 1.2);
                verticalAlignment: Text.AlignVCenter
                text: titleFail
            }

            Text {
                id: failMessage

                anchors {
                    top: failTitle.bottom
                    topMargin: 20
                    left: failTitle.left
                }

                font {
                    pixelSize: 20
                    bold: false
                }

                color: Qt.darker(parent.border.color, 1.2);
                verticalAlignment: Text.AlignVCenter
                text: messageFail
            }
        }
    }

    Timer {
        id: hideTimer

        interval: 4000

        onTriggered: resultState = 0;
    }

    function executeResult()
    {
        hideTimer.stop();
        resultState = 0;

        if (!globalUsers || !at) {
            return false;
        }

        var i;
        var len = globalUsers.count;
        var found = -1;

        for (i = 0; i < len; i++) {

            if (globalUsers.get(i).username === at.username.input && globalUsers.get(i).userpassword === at.userpassword.input) {

                found = i;
                break;
            }
        }

        if (accessAt) {

            accessAt.username = at.username;
            accessAt.userpassword = at.userpassword;
        }

        if (globalIndexTo) {
            globalIndexTo.value = found;
        }

        if (found <= 0) {
            resultState = 1;
        }
        else {
            resultState = 2;
        }

        hideTimer.start();
    }

    Component.onDestruction: {
        console.log("CheckGlobalAccessDelegate destructed <", objectName, ">");
    }
}
