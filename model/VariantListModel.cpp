#include "VariantListModel.h"

#include <QDebug>

int VariantListModel::m_registeredMetaType = 0;

VariantListModel::VariantListModel(QObject* parent)
    : QAbstractListModel(parent)
    , m_contentChangeBlocked(false)
{
    m_registeredMetaType = qRegisterMetaType<VariantListModel*>("VariantListModel*");
}

VariantListModel::~VariantListModel()
{
    qDebug() << "d'tor VariantListModel::VariantListModel <" << objectName() << "> called";
}

QVariantList VariantListModel::dataPool() const
{
    return m_dataPool;
}

const QVariantList& VariantListModel::poolData() const
{
    return m_dataPool;
}

void VariantListModel::setDataPool(const QVariantList& val)
{
    if (val == m_dataPool) {
        return;
    }

    beginResetModel();
    m_dataPool = val;
    endResetModel();

    emit dataPoolChanged();
    emitContentChanged();
}

void VariantListModel::emitContentChanged()
{
    if (!m_contentChangeBlocked) {
        emit contentChanged();
    }
}

void VariantListModel::mergeDataPool(const QVariantList& val)
{
    int poolLen = m_dataPool.length();
    int valLen = val.length();
    int i, j;

    bool hasChanges = false;

    m_contentChangeBlocked = true;

    for (i = 0, j = 0; i < poolLen && j < valLen; ++i, ++j) {

        if (val.at(j) != m_dataPool.at(i)) {

            QModelIndex indi = index(i);

            m_dataPool[i] = val.at(j);
            dataChanged(indi, indi, m_allRoles);
            hasChanges = true;
        }
    }

    for (; j < valLen; ++j, ++i) {

        append(val.at(j));
        hasChanges = true;
    }

    if (poolLen > valLen) {

        remove(i, poolLen - 1);
        hasChanges = true;
    }

    m_contentChangeBlocked = false;

    if (hasChanges) {
        emitContentChanged();
    }
}

QHash<int, QByteArray> VariantListModel::roleNames() const
{
    return m_roleNames;
}

void VariantListModel::setRoleNames(const QHash<int, QByteArray>& val)
{
    m_allRoles.clear();
    m_roleNames = val;

    QHash<int, QByteArray>::const_iterator it = m_roleNames.constBegin();

    while (it != m_roleNames.constEnd()) {

        m_allRoles.append(it.key());
        ++it;
    }

    m_allRoles << Qt::DisplayRole << Qt::EditRole;

    emit rolesChanged();
}

int VariantListModel::rowCount(const QModelIndex& parent) const
{
    if (parent.isValid()) {
        return 0;
    }

    return m_dataPool.length();
}

QVariant VariantListModel::data(const QModelIndex& index, int role) const
{
    QVariant result;

    if (!index.isValid()) {
        return result;
    }

    if (role != Qt::DisplayRole && !m_roleNames.contains(role)) {
        return result;
    }

    int row = index.row();

    if (row < 0 || row >= rowCount()) {
        return result;
    }

    if (role == Qt::DisplayRole) {
        result = m_dataPool.at(row);
    }
    else {

        QString key = m_roleNames.value(role);
        QVariant varValue = m_dataPool.at(row);
        QVariantMap valueMap = varValue.value<QVariantMap>();
        result = valueMap.value(key);
    }

    return result;
}

bool VariantListModel::setData(const QModelIndex &index, const QVariant& value, int role)
{
    if (!index.isValid()) {
        return false;
    }

    if (role != Qt::EditRole && !m_roleNames.contains(role)) {
        return false;
    }

    int row = index.row();

    if (row < 0 || row >= rowCount()) {
        return false;
    }

    QVector<int> changedRoles;

    if (role == Qt::EditRole) {

        if (value != m_dataPool[row]) {

            m_dataPool[row] = value;
            changedRoles << m_allRoles;
        }
    }
    else {

        QString key = m_roleNames.value(role);
        QVariant varValue = m_dataPool.at(row);
        QVariantMap valueMap = varValue.value<QVariantMap>();

        if (value != valueMap.value(key)) {

            valueMap[key] = value;
            varValue.setValue<QVariantMap>(valueMap);
            m_dataPool[row] = varValue;

            changedRoles << role;
        }
    }

    if (changedRoles.length() > 0) {

        emit dataChanged(index, index, changedRoles);
        emitContentChanged();
    }

    return changedRoles.length() > 0;
}

QVariant VariantListModel::get(int idx) const
{
    if (idx < 0 || idx >= m_dataPool.length()) {
        return QVariant();
    }

    return m_dataPool.at(idx);
}

void VariantListModel::insert(int idx, const QVariant& val)
{
    if (idx < 0) {
        idx = 0;
    }

    if (idx > m_dataPool.length()) {
        idx = m_dataPool.length();
    }

    beginInsertRows(QModelIndex(), idx, idx);
    m_dataPool.insert(idx, val);
    endInsertRows();

    emitContentChanged();
}

void VariantListModel::append(const QVariant& val)
{
    insert(m_dataPool.length(), val);
}

void VariantListModel::prepend(const QVariant& val)
{
    insert(0, val);
}

bool VariantListModel::remove(int idx)
{
    if (idx < 0 || idx >= m_dataPool.length()) {
        return false;
    }

    beginRemoveRows(QModelIndex(), idx, idx);
    m_dataPool.removeAt(idx);
    endRemoveRows();

    emitContentChanged();

    return true;
}

bool VariantListModel::remove(int from, int to)
{
    if (from > to) {
        return false;
    }
    if (from < 0 || from >= m_dataPool.length()) {
        return false;
    }

    if (to < 0 || to >= m_dataPool.length()) {
        return false;
    }

    int i;

    beginRemoveRows(QModelIndex(), from, to);
    for (i = to; i >= from; --i) {
        m_dataPool.removeAt(i);
    }
    endRemoveRows();

    emitContentChanged();

    return true;
}

bool VariantListModel::move(int from, int to)
{
    if (from < 0 || from >= m_dataPool.length()) {
        return false;
    }

    if (to < 0 || to >= m_dataPool.length()) {
        return false;
    }

    beginMoveRows(QModelIndex(), from, from, QModelIndex(), to);
    m_dataPool.move(from, to);
    endMoveRows();

    emitContentChanged();

    return true;
}
