import QtQuick 2.11
import "../common"
import "."

FocusRect {
    id: delegate
    objectName: "CUIFSlideDelegate"

    signal executeCommand(var from, var what, var args)

    property var at: null
    property var cuifvaluestyles: QtModel.mainAppModel.subAppSettings.general.cuifvaluestyles.value
    property var select: QtModel.mainAppModel.subAppSettings.general.style_select.value

    height: 500

    color: "transparent"

    UpDownSlider {

        anchors {
            fill: parent
            topMargin: 15
            bottomMargin: 5
        }

        at: delegate.at
        valueSlide: cuifvaluestyles[select].valueSlide
    }

    Component.onDestruction: {
        console.log("CUIFSlideDelegate destructed <", objectName, ">");
    }
}
