import QtQuick 2.11
import "../common"
import "."

FocusRect {
    id: delegate
    objectName: "CUIFSelectDelegate"

    signal executeCommand(var from, var what, var args)

    property string icon: ""
    property var at: null
    property var cuifvaluestyles: QtModel.mainAppModel.subAppSettings.general.cuifvaluestyles.value
    property var select: QtModel.mainAppModel.subAppSettings.general.style_select.value

    height: 100

    color: "transparent"

    CUIFValueSelect {

        anchors {
            centerIn: parent
        }
        height: 80

        icon: delegate.icon
        at: delegate.at
        valueSet: cuifvaluestyles[select].valueSet
    }

    Component.onDestruction: {
        console.log("CUIFSelectDelegate destructed <", objectName, ">");
    }
}
