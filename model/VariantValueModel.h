#ifndef VARIANTVALUEMODEL_H
#define VARIANTVALUEMODEL_H

#include <QQmlPropertyMap>
#include <QMetaType>

class VariantValueModel : public QQmlPropertyMap
{
    Q_OBJECT
public:
    explicit VariantValueModel(QObject* parent = nullptr);
    ~VariantValueModel() override;

    QVariantMap poolData() const;
    void setDataPool(const QVariantMap& dataMap);
    void mergeDataPool(const QVariantMap& val);

    static int registeredType()
    {
        return m_registeredMetaType;
    }

signals:
    void contentChanged();

private slots:
    void onValueChanged(const QString& key, const QVariant& value);

private:
    QString m_ownKey;

    static int m_registeredMetaType;
};

Q_DECLARE_METATYPE(VariantValueModel*)

#endif // VARIANTVALUEMODEL_H
